using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HandBoneType
{
    Thumb,
    Index,
    Middle,
    Ring,
    Pinky,
    Other,
}

[System.Flags]
public enum EvaluatedJoints
{
    None = 0x00000000,
    Thumb = 0x00000001,
    Index = 0x00000010,
    Middle = 0x00000100,
    Ring = 0x00001000,
    Pinky = 0x00010000,
    Other = 0x00100000,
    All = ~None,
}

public class HandStruct : ScriptableObject
{
    public HandStruct() => Initialize();
    public HandStruct(HandBonesData handBonesData) => Initialize(handBonesData);

    public HandStruct(HandStruct other)
    {
        this.handStructName = other.name;
        this.evaluatedJoints = other.evaluatedJoints;
        this.jointsTolerance = other.jointsTolerance;
        this.enableOrientationCheck = other.enableOrientationCheck;
        this.orientationTolerance = other.orientationTolerance;
        this.wrist = other.Wrist;
        this.bonesPositionsRelativeToWristCache = other.WristRelativePositionsCache;
        this.fingers = new List<FingerStruct>(other.fingers.Count);
        foreach (FingerStruct finger in other.fingers)
            fingers.Add(finger);
    }

    [Header("Recognition Details")]
    [SerializeField] private string handStructName;
    [HelpBox("Evaluated Joints is not used for now.", UnityEditor.MessageType.Warning), Space]
    [SerializeField, ReadOnly] public EvaluatedJoints evaluatedJoints = EvaluatedJoints.All;
    [Tooltip("Tolerance expressed as a the distance between target bone and hand bone")]
    [SerializeField, Min(0.0f)] public float jointsTolerance = 0.0385f;
    [SerializeField] public bool enableOrientationCheck = false;
    [Tooltip("Tolerance expressed as a the result of the dot product between the target orientation and the hand orientation")]
    [SerializeField, ConditionalHide("enableOrientationCheck", true)] public float orientationTolerance = 0.8f;
    [Space]
    [SerializeField] private TransformStruct wrist;
    [Space]
    [SerializeField] private List<FingerStruct> fingers;
    [Space]
    [SerializeField] private List<Vector3> bonesPositionsRelativeToWristCache = new List<Vector3>();

    public string HandStructName { get => handStructName; }
    public List<FingerStruct> Fingers { get => fingers; }
    public TransformStruct Wrist { get => wrist; }
    public FingerStruct GetFinger(HandBoneType fingerType) => fingers[(int)fingerType];
    public List<Vector3> WristRelativePositionsCache { get => bonesPositionsRelativeToWristCache; }

    [ContextMenu("Initialize")]
    public void Initialize()
    {
        Initialize(null, null);
    }

    public void Initialize(HandBonesData handBonesData = null, string name = null)
    {
        HandBoneType[] handBoneTypes = Enum.GetValues(typeof(HandBoneType)) as HandBoneType[];
        this.fingers = new List<FingerStruct>(handBoneTypes.Length);

        if (handBonesData == null)
        {
            // We populate every container as an empty one
            this.handStructName = "None";
            this.wrist = new TransformStruct("Wrist");
            foreach (HandBoneType handBoneType in handBoneTypes)
                this.fingers.Add(new FingerStruct(0, handBoneType.ToString()));
        }
        else
        {
            // We extract data from the HandBonesData to populate this HandStruct
            this.handStructName = (string.IsNullOrEmpty(name)) ? handBonesData.name : name;
            this.wrist = new TransformStruct(handBonesData.WristJoint, "Wrist");
            foreach (HandBoneType handBoneType in handBoneTypes)
                this.fingers.Add(new FingerStruct(handBonesData.GetBones(handBoneType), handBoneType.ToString()));
        }

        CalculatePositionsRelativeToWrist();
    }

    [ContextMenu("CalculatePositionsRelativeToWrist")]
    private void CalculatePositionsRelativeToWrist()
    {
        // We cache the position relative to the wrist of each bones (since they never change).
        bonesPositionsRelativeToWristCache = new List<Vector3>();
        foreach (FingerStruct finger in Fingers)
        {
            foreach (TransformStruct joint in finger.joints)
            {
                //Matrix4x4 version of Transform.InverseTransformPoint
                bonesPositionsRelativeToWristCache.Add(Wrist.WorldToLocalMatrix.MultiplyPoint3x4(joint.position));
            }
        }
    }

    private void OnValidate()
    {
        // Since we can't concatenate attribute, this is the only way to clamp the value.
        orientationTolerance = Mathf.Clamp01(orientationTolerance);
    }
}