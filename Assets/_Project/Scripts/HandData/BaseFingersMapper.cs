using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class BaseFingersMapper : MonoBehaviour
{
    [System.Serializable]
    public class JointPairs
    {
        public JointPairs(string name, Transform[] source = null, Transform[] target = null)
        {
            this.Name = name;
            sourceBones = source;
            targetBones = target;
        }

        public string Name = "";
        public Transform[] sourceBones = new Transform[0];
        public Transform[] targetBones = new Transform[0];

        //public Vector3 avatarForwardAxis = Vector3.forward;
        //public Vector3 avatarUpAxis = Vector3.up;
        //public Vector3 targetForwardAxis = Vector3.forward;
        //public Vector3 targetUpAxis = Vector3.up;
    }

    protected BaseFingersMapper()
    {
        HandBoneType[] handBoneTypes = Enum.GetValues(typeof(HandBoneType)) as HandBoneType[];
        this.jointPairsList = new JointPairs[handBoneTypes.Length];
        for (int i = 0; i < handBoneTypes.Length; ++i)
            jointPairsList[i] = new JointPairs(handBoneTypes[i].ToString());
    }

    [SerializeField] protected JointPairs[] jointPairsList;
    public JointPairs GetJointPairs(HandBoneType handBoneType) => jointPairsList[(int)handBoneType];

    protected abstract void InitializePairs();
}