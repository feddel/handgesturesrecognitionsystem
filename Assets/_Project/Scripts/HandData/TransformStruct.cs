using UnityEngine;

[System.Serializable]
public struct TransformStruct
{
    [System.Serializable]
    private struct TransformDetails
    {
        [SerializeField] public Vector3 forward;
        [SerializeField] public Vector3 right;
        [SerializeField] public Vector3 up;
        [SerializeField] public Matrix4x4 worldToLocalMatrix;
        [SerializeField] public Matrix4x4 localToWorldMatrix;
    }

    public TransformStruct(string transformName = null)
    {
        this.transformName = (transformName == null) ? "NoName" : transformName;
        this.position = Vector3.zero;
        this.rotation = Quaternion.identity;
        this.localPosition = Vector3.zero;
        this.localRotation = Quaternion.identity;
        this.transformDetails.forward = Vector3.zero;
        this.transformDetails.right = Vector3.zero;
        this.transformDetails.up = Vector3.zero;
        this.transformDetails.worldToLocalMatrix = Matrix4x4.zero;
        this.transformDetails.localToWorldMatrix = Matrix4x4.zero;
    }

    public TransformStruct(Transform transform, string transformName = null)
    {
        this.transformName = (transformName == null) ? transform.name : transformName;
        this.position = transform.position;
        this.rotation = transform.rotation;
        this.localPosition = transform.localPosition;
        this.localRotation = transform.localRotation;
        this.transformDetails.forward = transform.forward;
        this.transformDetails.right = transform.right;
        this.transformDetails.up = transform.up;
        this.transformDetails.worldToLocalMatrix = transform.worldToLocalMatrix;
        this.transformDetails.localToWorldMatrix = transform.localToWorldMatrix;
    }

    public TransformStruct(TransformStruct other, string transformName = null)
    {
        this.transformName = (transformName == null) ? other.transformName : transformName;
        this.position = other.position;
        this.rotation = other.rotation;
        this.localPosition = other.localPosition;
        this.localRotation = other.localRotation;
        this.transformDetails = other.transformDetails;
    }

    [SerializeField] private string transformName;
    public Vector3 position;
    public Vector3 localPosition;
    public Quaternion rotation;
    public Quaternion localRotation;
    [SerializeField] private TransformDetails transformDetails;

    public string TransformName { get => transformName; }
    public Vector3 ForwardVector { get => transformDetails.forward; private set => transformDetails.forward = value; }
    public Vector3 RightVector { get => transformDetails.right; private set => transformDetails.right = value; }
    public Vector3 UpVector { get => transformDetails.up; private set => transformDetails.up = value; }
    public Matrix4x4 WorldToLocalMatrix { get => transformDetails.worldToLocalMatrix; }
    public Matrix4x4 LocalToWorldMatrix { get => transformDetails.localToWorldMatrix; }
}