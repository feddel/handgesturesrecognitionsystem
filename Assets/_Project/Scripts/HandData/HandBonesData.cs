using System;
using System.Collections.Generic;
using UnityEngine;

public class HandBonesData : MonoBehaviour
{
    [SerializeField] private Transform root = null;
    [SerializeField] private OVRSkeleton.SkeletonType handType = OVRSkeleton.SkeletonType.None;
    [Space]
    [SerializeField] private Transform wristJoint = null;
    [SerializeField] private List<Transform> thumbJoints = new List<Transform>();
    [SerializeField] private List<Transform> indexJoints = new List<Transform>();
    [SerializeField] private List<Transform> middleJoints = new List<Transform>();
    [SerializeField] private List<Transform> ringJoints = new List<Transform>();
    [SerializeField] private List<Transform> pinkyJoints = new List<Transform>();
    [SerializeField] private List<Transform> otherJoints = new List<Transform>();

    public OVRSkeleton.SkeletonType HandType { get => handType; }
    public Transform WristJoint { get => wristJoint; private set => wristJoint = value; }
    public List<Transform> GetBones(HandBoneType handBoneType)
    {
        switch (handBoneType)
        {
            case HandBoneType.Thumb: return thumbJoints;
            case HandBoneType.Index: return indexJoints;
            case HandBoneType.Middle: return middleJoints;
            case HandBoneType.Ring: return ringJoints;
            case HandBoneType.Pinky: return pinkyJoints;
            case HandBoneType.Other: return otherJoints;
            default: return null;
        }
    }

    private void Reset()
    {
        wristJoint = null;
        thumbJoints.Clear();
        indexJoints.Clear();
        middleJoints.Clear();
        ringJoints.Clear();
        pinkyJoints.Clear();
        otherJoints.Clear();
    }

    /// <summary>
    /// Automatically populate the containers. Every child of 'root' will be consider.
    /// </summary>
    [ContextMenu("AutoPopulate")]
    private void AutoPopulate()
    {
        if (root == false)
        {
            Debug.LogError("No root has been assigned.", this);
            return;
        }

        // Clean every data
        Reset();

        // We look through all child recursively
        Transform[] children = root.GetComponentsInChildren<Transform>();
        int childCount = children.Length;
        bool wristFound = false;
        for (int i = 0; i < childCount; i++)
        {
            Transform child = children[i];
            if (child == null || child.name == null || child == this.transform || child == root || ShouldIgnoreJoint(child)) continue;

            // Depending on the name, we assign the bone to the corresponding container
            string formattedName = child.name.ToLower();
            if (formattedName.Contains("thumb"))
            {
                this.thumbJoints.Add(child);
            }
            else if (formattedName.Contains("index"))
            {
                this.indexJoints.Add(child);
            }
            else if (formattedName.Contains("middle"))
            {
                this.middleJoints.Add(child);
            }
            else if (formattedName.Contains("ring"))
            {
                this.ringJoints.Add(child);
            }
            else if (formattedName.Contains("pinky"))
            {
                this.pinkyJoints.Add(child);
            }
            else if (wristFound == false && formattedName.Contains("wrist") || (formattedName.EndsWith("hand") && child.childCount > 3))
            {
                this.wristJoint = child;
                wristFound = true;
            }
            else
            {
                this.otherJoints.Add(child);
            }
        }
    }

    /// <summary>
    /// Filter out bones that we are not interested in
    /// </summary>
    /// <param name="theJoint"></param>
    /// <returns></returns>
    private bool ShouldIgnoreJoint(Transform theJoint)
    {
        if (theJoint.gameObject.activeSelf == false) return true;

        // Don't store the handsmodel location. This is only for rendering
        string loweredName = theJoint.name.ToLower();

        if (loweredName == "handsmodel" || loweredName == "lhand" || loweredName == "rhand" ||
            loweredName.StartsWith("tip_collider") || loweredName.StartsWith("hands_col") || loweredName.Contains("mesh") ||
            loweredName.Contains("marker") || loweredName.Contains("stub") || loweredName.StartsWith("coll_"))
            return true;

        return false;
    }
}