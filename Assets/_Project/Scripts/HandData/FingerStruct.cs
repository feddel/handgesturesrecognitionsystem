using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct FingerStruct
{
    public FingerStruct(int numJoints, string name = "")
    {
        this.Name = name;
        joints = new List<TransformStruct>(numJoints);

        for (int i = 0; i < numJoints; ++i)
            joints.Add(new TransformStruct());
    }

    public FingerStruct(List<Transform> transforms, string name = "")
    {
        this.Name = name;
        int count = transforms.Count;
        joints = new List<TransformStruct>(count);

        for (int i = 0; i < count; ++i)
            joints.Add(new TransformStruct(transforms[i]));
    }

    [SerializeField] private string Name;
    public List<TransformStruct> joints;
}