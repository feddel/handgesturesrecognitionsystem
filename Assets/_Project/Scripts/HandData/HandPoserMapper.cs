using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is used to copy the position of the user hand into a another target hand. Mostly used for debug purposes.
/// </summary>
public class HandPoserMapper : BaseFingersMapper
{
    [Flags]
    private enum VisualizedHand
    { None, Source, Target, }

    [SerializeField] private HandBonesData sourceHand;
    [SerializeField] private HandBonesData targetHand;
    [SerializeField] private bool updateWrist = false;
    [Header("Debug")]
    [SerializeField] private VisualizedHand visualizedHand = VisualizedHand.Target;
    [SerializeField] private float jointSize = 0.005f;

    private void Awake() => InitializePairs();

    public void Tick()
    {
        if (targetHand == null || sourceHand == null) return;
        UpdatePairs();
        VisualizeDebugBones();
    }

    /// <summary>
    /// Read bones data from a Source Hand and apply the local rotation of each on of the them to the Target Hand
    /// </summary>
    private void UpdatePairs()
    {
        // Update wrist
        if (updateWrist == true)
            targetHand.WristJoint.transform.rotation = sourceHand.WristJoint.transform.rotation;

        for (int i = 0; i < jointPairsList.Length - 1; i++)
        {
            JointPairs finger = jointPairsList[i];
            if (finger == null) continue;

            // We go through each bones and we copy the localRotation
            for (int j = 0; j < finger.targetBones.Length - 1; j++)
            {
                Transform sourceFinger = finger.sourceBones[j];
                Transform targetFinger = finger.targetBones[j];

                if (sourceFinger == null || targetFinger == null) continue;
                targetFinger.transform.localRotation = sourceFinger.transform.localRotation;

                //// Get the relative rotation from the current rotation to the target rotation
                //Quaternion f = Quaternion.Inverse(to.rotation) * from.rotation;

                //// Append relative rotation
                //finger.destinationBones[i].rotation *= f;
            }
        }
    }

    /// <summary>
    /// Automatically pairs bones from the source to the target ones.
    /// </summary>
    [ContextMenu("InitializePairs")]
    protected override void InitializePairs()
    {
        // We extract the number of fingers we need to consider from HandBoneType enum
        HandBoneType[] handBoneTypes = Enum.GetValues(typeof(HandBoneType)) as HandBoneType[];
        // We initialize an array based on the result
        jointPairsList = new JointPairs[handBoneTypes.Length];

        // We populate the array with the data from each bone
        foreach (HandBoneType handBoneType in handBoneTypes)
        {
            if (sourceHand != null && targetHand != null)
            {
                Transform[] source = sourceHand.GetBones(handBoneType).ToArray();
                Transform[] target = targetHand.GetBones(handBoneType).ToArray();
                jointPairsList[(int)handBoneType] = new JointPairs(handBoneType.ToString(), source, target);
            }
        }
    }

    private void VisualizeDebugBones()
    {
        if (visualizedHand == VisualizedHand.None || jointPairsList == null) return;

        for (int i = 0; i < jointPairsList.Length; i++)
        {
            JointPairs temp = jointPairsList[i];

            if (visualizedHand.HasFlag(VisualizedHand.Source) && temp.sourceBones != null)
                DrawBones(temp.sourceBones, Color.red);

            if (visualizedHand.HasFlag(VisualizedHand.Target) && temp.targetBones != null)
                DrawBones(temp.targetBones, Color.yellow);
        }
    }

    private void DrawBones(Transform[] transforms, Color color)
    {
        for (int i = 0; i < transforms.Length; i++)
        {
            // Draw joints
            if (transforms[i] == null) continue;
            LineDrawer.DrawLine(transforms[i].position + transforms[i].forward * jointSize,
                transforms[i].position + transforms[i].forward * -jointSize, color);

            // Draw bones
            if (i < transforms.Length - 1)
            {
                if (transforms[i] == null || transforms[i + 1] == null) continue;
                LineDrawer.DrawLine(transforms[i].position, transforms[i + 1].position, color);
            }
        }
    }
}