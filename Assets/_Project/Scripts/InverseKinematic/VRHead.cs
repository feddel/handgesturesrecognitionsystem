using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRHead : MonoBehaviour
{
    [SerializeField] private Transform rootObject, followObject;
    [SerializeField] private Vector3 headBodyOffset;
    [SerializeField] private Vector3 rotationOffset;
    [SerializeField] private Vector3 positionOffset;
    [SerializeField] private bool lockPosition = false;
    [SerializeField] private bool lockRotation = false;

    // Update is called once per frame
    public void Tick()
    {
        if (lockPosition == false)
        {
            rootObject.position = transform.position - headBodyOffset;
            transform.position = followObject.TransformPoint(positionOffset);
        }

        transform.rotation = followObject.rotation * Quaternion.Euler(rotationOffset);

        if (lockRotation == false)
        {
            rootObject.forward = Vector3.ProjectOnPlane(followObject.up, Vector3.up).normalized;
        }
    }
}