using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VRMap
{
    [SerializeField] private Transform vrTarget;
    [SerializeField] private Transform rigTarget;
    [SerializeField] private Vector3 trackingPositionOffset;
    [SerializeField] private Vector3 trackingRotationOffset;

    public void Map()
    {
        rigTarget.position = vrTarget.TransformPoint(trackingPositionOffset);
        rigTarget.rotation = vrTarget.rotation * Quaternion.Euler(trackingRotationOffset);
    }
}

public class VRRig : MonoBehaviour
{
    [System.Flags]
    private enum Followers
    {
        None = 0x00000000,
        Head = 0x00000001,
        LeftHand = 0x00000010,
        RightHand = 0x00000100,
        Body = 0x00001000,
    }

    [SerializeField] private Followers followers = ~Followers.None;
    [SerializeField] private VRHead head;
    [SerializeField] private VRMap leftHand;
    [SerializeField] private VRMap rightHand;

    private void LateUpdate()
    {
        if (followers.HasFlag(Followers.Head)) head.Tick();
        if (followers.HasFlag(Followers.LeftHand)) leftHand.Map();
        if (followers.HasFlag(Followers.RightHand)) rightHand.Map();
    }
}