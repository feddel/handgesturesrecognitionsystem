using UnityEngine;

public class HandsManager : MonoBehaviour
{
#pragma warning disable 0414
    [TextArea(3, 10), SerializeField]
    private string componentDescription = "This component is responsible to get all hands in the" +
    " scene and check if they are tracked properly. It dispatch an event whenever a hand is not being tracked properly" +
    "and whenever a hand is being tracked again.";
#pragma warning restore 0414

    [Header("Settings")]
    [SerializeField] private Camera mainCamera = null;
    [Header("Debug")]
    [SerializeField] private bool debugMode = false;

    private VRController[] controllers;
    private VRController leftController;
    private VRController rightController;

    public VRController GetRightStateMachine { get => rightController; }
    public VRController GetLeftStateMachine { get => leftController; }
    public OVRHand GetRightHand { get => rightController?.Hand; }
    public OVRHand GetLeftHand { get => leftController?.Hand; }
    public Camera MainCamera { get => mainCamera; }

    public delegate void OnTrackingStateChange(VRController controller, bool enable);
    public event OnTrackingStateChange onTrackingStateChange;

    private static HandsManager instance = null;
    public static HandsManager Instance { get => instance; }

    private void Start()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;

        controllers = FindObjectsOfType<VRController>();
        foreach (VRController controller in controllers)
        {
            if (controller.Skeleton.GetSkeletonType() == OVRSkeleton.SkeletonType.HandRight)
                rightController = controller;
            else
                leftController = controller;

            onTrackingStateChange += controller.SetEnable;
        }

        mainCamera = (MainCamera == null) ? Camera.main : MainCamera;
    }

    private void Update()
    {
        SendTrackingMessage();
    }

    private void SendTrackingMessage()
    {
        // We keep track of which hand need to be updated
        foreach (VRController controller in controllers)
        {
            bool isControllerTracked = IsControllerTracked(controller);
            if (isControllerTracked == true && controller.PreviousTrackingState == false)
            {
                onTrackingStateChange?.Invoke(controller, true);

                if (debugMode)
                    Debug.Log("Tracked");
            }
            else if (isControllerTracked == false && controller.PreviousTrackingState == true)
            {
                onTrackingStateChange?.Invoke(controller, false);

                if (debugMode)
                    Debug.Log("Lost track");
            }
        }
    }

    public OVRHand GetHand(OVRSkeleton.SkeletonType handType)
    {
        switch (handType)
        {
            case OVRSkeleton.SkeletonType.HandLeft:
                return GetLeftHand;

            case OVRSkeleton.SkeletonType.HandRight:
                return GetRightHand;

            case OVRSkeleton.SkeletonType.None:
            default:
                return null;
        }
    }

    private bool IsVisible(Camera camera, Renderer renderer)
    {
        // We determine the camera frustum planes
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        // We do an AABB test to see if the bounds of the renderer is withing the frustum
        return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
    }

    private bool IsTrackedProperly(OVRHand hand)
    {
        return (hand.HandConfidence == OVRHand.TrackingConfidence.High && hand.IsDataHighConfidence == true && hand.IsTracked);
    }

    private bool IsControllerTracked(VRController controller)
    {
        return (IsVisible(mainCamera, controller.HandRenderer) && IsTrackedProperly(controller.Hand));
    }
}