using System;
using System.Collections.Generic;
using UnityEngine;

public class HandGesturesRecognizer : MonoBehaviour
{
#pragma warning disable 0414
    [TextArea(3, 10), SerializeField]
    private string componentDescription = "This component is responsible to get all state machines in the" +
        " scene and perform the gestures detection for them. It is also responsible to call the Tick function inside HandPoserMapper (debug tool)";
#pragma warning restore 0414

    [Header("Settings")]
    [SerializeField] private bool debugMode = false;
    [SerializeField] private Camera mainCamera = null;
    [SerializeField] private HandState neutralState = null;

    [Tooltip("After how many frames the gesture detection should take place. " +
        "High values will improve performance but will create a clear delay from the user perspective. Low values will save less performance but make the delay less evident.")]
    [SerializeField] private byte frequency = 10;

    [Tooltip("Expressed as the result of a Dot Product.")]
    [Range(0.0f, 1.0f), SerializeField] private float fallbackOrientationTolerance = 0.8f;

    [Tooltip("Expressed as the Distance between two points.")]
    [Min(0.0f), SerializeField] private float fallbackPositionTolerance = 0.0385f;

    private static byte currentFrameCount = 0;
    private HandGestureStateMachine[] handGestureStateMachines = null;
    private HandPoserMapper[] handPoserMappers = null;

    // Singleton instance
    private static HandGesturesRecognizer instance = null;
    public HandGesturesRecognizer Instance { get => instance; }

    private void Awake()
    {
        // This ensure the Singleton pattern to work properly
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    private void Start()
    {
        // Initialize singleton
        instance = Instance;

        // Find all HandGestureStateMachines
        handGestureStateMachines = FindObjectsOfType<HandGestureStateMachine>();
        if (handGestureStateMachines == null)
        {
            Debug.LogWarning("No HandGesturesStateMachines detected in the scene.", this);
            return;
        }

        // Initialize all HandGestureStateMachines
        foreach (HandGestureStateMachine handGestureStateMachine in handGestureStateMachines)
            handGestureStateMachine.Initialize(neutralState);

        mainCamera = (mainCamera == null) ? Camera.main : mainCamera;

        // Find all HandPosers for debug purpose
        handPoserMappers = FindObjectsOfType<HandPoserMapper>();
        if (handPoserMappers != null)
        {
            foreach (HandPoserMapper handPoserMapper in handPoserMappers)
                handPoserMapper.enabled = debugMode;
        }
    }

    private void Update()
    {
        // We avoid performing gesture detection every frame to improve performance
        currentFrameCount++;
        if (frequency == 0 || currentFrameCount % frequency == 0)
        {
            for (int i = 0; i < handGestureStateMachines.Length; ++i)
            {
                HandGestureStateMachine currentStateMachine = handGestureStateMachines[i];
                if (currentStateMachine != null && currentStateMachine.enabled == true)
                    DetectGesture(currentStateMachine);
            }
        }

        // We update HandPoserMappers
        if (debugMode)
        {
            foreach (HandPoserMapper handPoserMapper in handPoserMappers)
                handPoserMapper.Tick();
        }
    }

    /// <summary>
    /// Detect when a gesture is being performed and if it's different from the current one it trigger a change of state.
    /// </summary>
    private void DetectGesture(HandGestureStateMachine handGestureStateMachine)
    {
        // Get the HandPose that is closest to the one being performed by the user currently.
        HandStruct performedGesture = GetPerformedGesture(handGestureStateMachine);
        // Get the HandState corresponding to the HandPose
        HandState newState = ExtractState(performedGesture, handGestureStateMachine.CurrentState.PossibleTransitions);

        // Check if we recognized a valid HandStruct and it's different than the previous one
        if (handGestureStateMachine.CurrentState.name != newState.name)
            handGestureStateMachine.ChangeState(newState);
    }

    /// <summary>
    /// Return the closest HandPose to the gesture currently being performed by the user.
    /// </summary>
    private HandStruct GetPerformedGesture(HandGestureStateMachine handGestureStateMachine)
    {
        // Set current HandStruct to default
        HandStruct performedGesture = neutralState.AssociatedHandStruct;
        HandState currentHandState = handGestureStateMachine.CurrentState;
        HandBonesData handBonesData = handGestureStateMachine.BonesData;
        float currentMin = Mathf.Infinity;

        // Check which HandStruct is the most similar to the one performed by the user
        foreach (HandState currentState in currentHandState.PossibleTransitions)
        {
            if (currentState.AssociatedHandStruct == null)
            {
                Debug.LogWarning($"{currentState} doesn't have an associated Hand Struct.");
                continue;
            }

            // The neutral state have no positions relative to the wrist, so we skip it
            if (currentState.AssociatedHandStruct.WristRelativePositionsCache.Count == 0) continue;

            float sumDistance = 0;
            bool isDiscarded = false;

            // Check if we need to compare the orientation as well
            if (currentState.AssociatedHandStruct.enableOrientationCheck == true)
            {
                // We flip the forward vector if it's a left hand and we calculate the forward vector to be relative to the camera
                Vector3 handOrientationVector = GetOrientationVector(handBonesData.WristJoint.forward, handBonesData.HandType);
                Vector3 cameraRelativeOrientation = GetCameraRelativeOrientationVector(handOrientationVector);
                // Check if the orientation is acceptable, if not discard the comparison
                if (IsOrientationValid(currentState.AssociatedHandStruct, cameraRelativeOrientation) == false) continue;
            }

            // We collect relative positions from the HandStruct and from the HandBonesData
            List<Vector3> handPoseWristRelativePositions = ExtractLocationsRelativeToWrist(currentState.AssociatedHandStruct);
            List<Vector3> userHandWristRelativePositions = ExtractLocationsRelativeToWrist(handBonesData);

            // We look for the least amount of bones between the HandBonesData and HandStruct (in order to avoid out of range crashes).
            int structBoneCount = handPoseWristRelativePositions.Count;
            int userBonesCount = userHandWristRelativePositions.Count;
            int count = userBonesCount;
            if (structBoneCount != userBonesCount)
            {
                // If this happen, we notify the user.
                Debug.LogWarning($"{currentState.AssociatedHandStruct} and {handBonesData} have a different amount of bones. Picking the smallest amount: {structBoneCount}, {userBonesCount}");
                count = (structBoneCount < userBonesCount) ? structBoneCount : userBonesCount;
            }

            for (int i = 0; i < count; ++i)
            {
                // Squared distance is more efficient than Distance because we don't need the exact distance value
                Vector3 distanceVector = handPoseWristRelativePositions[i] - userHandWristRelativePositions[i];
                float sqrDistance = distanceVector.sqrMagnitude;

                // If the distance exceed the tolerance, the user fingers position
                // are not close enough to the currently evaluated pose
                float toleranceValue = GetDistanceToleranceValue(currentState.AssociatedHandStruct);
                if (sqrDistance > toleranceValue)
                {
                    isDiscarded = true;
                    break;
                }
                sumDistance += sqrDistance;
            }

            // If we don't discard the HandStruct, it mean it's a valid one
            // and if the sumDistance is less then the current one, it mean
            // that it's very close to the HandStruct the user is performing
            if (isDiscarded == false && sumDistance < currentMin)
            {
                currentMin = sumDistance;
                performedGesture = currentState.AssociatedHandStruct;
            }
        }
        return performedGesture;
    }

    /// <summary>
    /// Return HandState that correspond to the HandStruct passed as parameter.
    /// </summary>
    private HandState ExtractState(HandStruct handStruct, List<HandState> possibleTransitions)
    {
        foreach (HandState state in possibleTransitions)
        {
            if (state.AssociatedHandStruct == handStruct)
                return state;
        }
        return neutralState;
    }

    private float GetOrientationToleranceValue(HandStruct handStruct)
    {
        return (handStruct.orientationTolerance.IsNearlyZero()) ? fallbackOrientationTolerance : handStruct.orientationTolerance;
    }

    private float GetDistanceToleranceValue(HandStruct handStruct)
    {
        // Since we are considering the squared distance, we need to multiply the selected tolerance by itself
        float tolerance = (handStruct.jointsTolerance.IsNearlyZero()) ? fallbackPositionTolerance : handStruct.jointsTolerance;
        return tolerance * tolerance;
    }

    private Vector3 GetCameraRelativeOrientationVector(Vector3 forwardVector)
    {
        return mainCamera.transform.InverseTransformDirection(forwardVector);
    }

    private Vector3 GetOrientationVector(Vector3 forwardVector, OVRSkeleton.SkeletonType handType)
    {
        return (handType == OVRSkeleton.SkeletonType.HandRight) ? forwardVector : -forwardVector;
    }

    private bool IsOrientationValid(HandStruct handStruct, Vector3 forwardVector)
    {
        float tolerance = GetOrientationToleranceValue(handStruct);
        float dot = Vector3.Dot(handStruct.Wrist.ForwardVector, forwardVector);
        return ((dot < tolerance) == false);
    }

    private List<Vector3> ExtractLocationsRelativeToWrist(HandBonesData handBonesData)
    {
        float flip = (handBonesData.HandType == OVRSkeleton.SkeletonType.HandLeft) ? -1.0f : 1.0f;
        List<Vector3> result = new List<Vector3>();
        HandBoneType[] handBoneTypes = (HandBoneType[])Enum.GetValues(typeof(HandBoneType));
        foreach (HandBoneType handBoneType in handBoneTypes)
        {
            foreach (Transform bone in handBonesData.GetBones(handBoneType))
                result.Add(handBonesData.WristJoint.InverseTransformPoint(bone.position) * flip);
        }
        return result;
    }

    private List<Vector3> ExtractLocationsRelativeToWrist(HandStruct handStruct)
    {
        return handStruct.WristRelativePositionsCache;
    }
}