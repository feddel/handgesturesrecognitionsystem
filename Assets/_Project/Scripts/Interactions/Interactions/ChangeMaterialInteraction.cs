using UnityEngine;

public class ChangeMaterialInteraction : Interaction
{
    [SerializeField] private Material[] materials;
    private bool isFirst = true;

    private Vector3 originalScale;

    private void Awake()
    {
        originalScale = this.transform.localScale;
    }

    public override void OnHoverEnter(HandState state)
    {
    }

    public override void OnHoverExit(HandState state)
    {
    }

    public override void OnSelection(HandState state)
    {
        switch (state.HandType)
        {
            case OVRSkeleton.SkeletonType.None:
            case OVRSkeleton.SkeletonType.HandLeft:
                ChangeScale();
                break;

            case OVRSkeleton.SkeletonType.HandRight:
                ChangeMaterial();
                break;

            default:
                break;
        }
    }

    public override void OnDeselection(HandState state)
    {
    }

    public void ChangeMaterial()
    {
        MeshRenderer target = GetComponent<MeshRenderer>();
        Material newMat = (isFirst) ? materials[1] : materials[0];
        target.material = newMat;
        isFirst = !isFirst;
    }

    private void ChangeScale()
    {
        this.transform.localScale *= (isFirst) ? 2.0f : 0.5f;
    }

    public void ScaleObject(float value)
    {
        this.transform.localScale = originalScale * (1.1f - value);
    }
}