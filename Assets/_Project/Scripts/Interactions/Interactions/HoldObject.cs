using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

//                                                                  May be game changer, I can use it just like any UI and make it react to clicks and inputs...
public class HoldObject : Interaction, /* ===============> */ IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler // <===============
{
    public override void OnHoverEnter(HandState state)
    {
    }

    public override void OnHoverExit(HandState state)
    {
    }

    public override void OnSelection(HandState state)
    {
        this.transform.parent = Camera.main.transform;
    }

    public override void OnDeselection(HandState state)
    {
        if (this.transform.parent != null)
            this.transform.parent = null;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    }
}