using UnityEngine;

public abstract class Interaction : MonoBehaviour, IInteractable
{
    public abstract void OnHoverEnter(HandState state);

    public abstract void OnHoverExit(HandState state);

    public abstract void OnSelection(HandState state);

    public abstract void OnDeselection(HandState state);
}