using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HandsInputModule : BaseInputModule
{
    public override void Process()
    {
        // This is left empty because we want to have more control on when the Input Module processes take place
    }

    // Should be called instead of Process
    public void ProcessPointer(PointerEventData eventData, GameObject pointedObject, OVRInput.Button button)
    {
        HandlePointerExitAndEnter(eventData, pointedObject);
        ProcessPress(eventData, pointedObject, button);
        ProcessDown(eventData, pointedObject, button);
        ProcessRelease(eventData, pointedObject, button);
    }

    private void ProcessPress(PointerEventData eventData, GameObject pointedObject, OVRInput.Button button)
    {
        if (OVRInput.GetDown(button))
        {
            // We retrieve the click and begin drag handlers
            eventData.pointerPressRaycast = eventData.pointerCurrentRaycast;
            eventData.pointerPress = ExecuteEvents.GetEventHandler<IPointerClickHandler>(pointedObject);
            eventData.pointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(pointedObject);

            // We execute the down and begin drag events
            ExecuteEvents.Execute(eventData.pointerPress, eventData, ExecuteEvents.pointerDownHandler);
            ExecuteEvents.Execute(eventData.pointerDrag, eventData, ExecuteEvents.beginDragHandler);
        }
    }

    private void ProcessDown(PointerEventData eventData, GameObject pointedObject, OVRInput.Button button)
    {
        if (OVRInput.Get(button))
            // We execute the drag event
            ExecuteEvents.Execute(eventData.pointerDrag, eventData, ExecuteEvents.dragHandler);
    }

    private void ProcessRelease(PointerEventData eventData, GameObject pointedObject, OVRInput.Button button)
    {
        if (OVRInput.GetUp(button))
        {
            // We execute the click event if the object we pressed is the same as when we release
            GameObject pointerRelease = ExecuteEvents.GetEventHandler<IPointerClickHandler>(pointedObject);
            if (eventData.pointerPress == pointerRelease)
                ExecuteEvents.Execute(eventData.pointerPress, eventData, ExecuteEvents.pointerClickHandler);

            // We execute the up and end drag event
            ExecuteEvents.Execute(eventData.pointerPress, eventData, ExecuteEvents.pointerUpHandler);
            ExecuteEvents.Execute(eventData.pointerDrag, eventData, ExecuteEvents.endDragHandler);

            // We clear the Event System selected game object
            eventSystem.SetSelectedGameObject(null);

            // We reset the event data
            eventData.pointerPress = null;
            eventData.pointerDrag = null;
            eventData.pointerCurrentRaycast.Clear();
        }
    }
}