using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer), typeof(HandPointer))]
public class PointerLineRenderer : MonoBehaviour
{
    [HelpBox("To adjust the width curve of the LineRenderer, use the Width graph in the LineRenderer component inspector.", UnityEditor.MessageType.Info), Space]
    [SerializeField] private bool enableLineRender = true;
    [SerializeField] private Gradient colorGradient;
    [SerializeField] private Color normalColor = Color.white;
    [SerializeField] private Color hitColor = Color.cyan;
    [SerializeField] private Material material = null;
    [SerializeField] private GameObject dot = null;
    [SerializeField] private float dotOffset = 0.0125f;
    [SerializeField, Range(0.0f, 1.0f)] private float dotScaleDownMultiplier = 0.66f;
    [SerializeField] private bool lockToTargetPosition = false;

    private Vector3 dotOriginalScale;
    private LineRenderer lineRender;
    private GradientColorKey[] normalColorKeys;
    private GradientColorKey[] hitColorKeys;
    private HandPointer handPointer;

    private void Awake()
    {
        handPointer = GetComponent<HandPointer>();

        // Setting up some default LineRenderer value.
        lineRender = GetComponent<LineRenderer>();
        lineRender.material = material;
        lineRender.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        lineRender.receiveShadows = false;
        lineRender.shadowBias = 0.0f;
        lineRender.useWorldSpace = true;

        // Detach pointer to make it independent from parent location and rotation.
        dotOriginalScale = dot.transform.localScale;
        dot.transform.parent = null;

        // Create colors used for the LineRenderer hit and normal state.
        normalColorKeys = new GradientColorKey[2] { new GradientColorKey(normalColor, 0.0f), new GradientColorKey(normalColor, 1.0f) };
        hitColorKeys = new GradientColorKey[2] { new GradientColorKey(hitColor, 0.0f), new GradientColorKey(hitColor, 1.0f) };

        handPointer.onEnableEvent += Enable;
        handPointer.onDisableEvent += Disable;
    }

    private void Update()
    {
        if (enableLineRender == false) return;

        SetLinePoints();
        SetLineColor(handPointer.HasHit);
        SetDotLocationAndOrientation(handPointer.HasHit && handPointer.InteractionMode == PointerInteractionMode.UI);
        SetDotScale();
    }

    private void SetDotLocationAndOrientation(bool isActive)
    {
        dot.SetActive(isActive);
        dot.transform.position = handPointer.HitPoint - handPointer.PointerDirection * dotOffset;
        dot.transform.rotation = Quaternion.LookRotation(dot.transform.position - Camera.main.transform.position);
    }

    private void SetLinePoints()
    {
        lineRender.SetPosition(0, handPointer.PointerLocation);
        float distance = (handPointer.HasHit == true) ? handPointer.HitDistance : handPointer.defaultDistance;
        if (handPointer.InteractionMode != PointerInteractionMode.UI && handPointer.HasHit && lockToTargetPosition == true)
            lineRender.SetPosition(1, handPointer.HitObject.transform.position);
        else
            lineRender.SetPosition(1, handPointer.PointerLocation + handPointer.PointerDirection * distance);
    }

    private void SetLineColor(bool hasHit)
    {
        colorGradient.colorKeys = (hasHit) ? hitColorKeys : normalColorKeys;
        lineRender.colorGradient = colorGradient;
    }

    private void SetDotScale()
    {
        float pinchValue = handPointer.Hand.GetFingerPinchStrength(OVRHand.HandFinger.Index);
        float scaleDownValue = 1.0f - (pinchValue * dotScaleDownMultiplier);
        dot.transform.localScale = dotOriginalScale * scaleDownValue;
    }

    private void Enable() => this.enabled = true;
    private void Disable() => this.enabled = false;

    private void OnEnable()
    {
        if (enableLineRender == false)
        {
            OnDisable();
            return;
        }

        dot.SetActive(true);
        lineRender.enabled = true;
    }

    private void OnDisable()
    {
        if (dot)
            dot.SetActive(false);
        lineRender.enabled = false;
    }

    private void OnDestroy()
    {
        handPointer.onEnableEvent -= Enable;
        handPointer.onDisableEvent -= Disable;
    }
}