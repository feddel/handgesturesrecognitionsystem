using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is meant to be used by the pointer as a pivot point to improve stability when pointing at something
/// https://www.youtube.com/watch?v=or5M01Pcy5U&t=1075s (Hand Tracking: Designing a New Input Modality)
/// </summary>
public class ShouldersManager : MonoBehaviour
{
    [SerializeField] private bool debugMode;
    [SerializeField, ConditionalHide("debugMode", true)] private Color lineColor = Color.yellow;
    [SerializeField] private Transform head = null;

    [SerializeField] private Transform leftShoulder;
    [SerializeField] private Transform rightShoulder;

    private void Awake()
    {
        if (head == null)
            head = this.transform.parent;

        this.transform.parent = null;
    }

    private void Update()
    {
        // We only want to follow the pitch rotation
        this.transform.position = head.position;
        this.transform.rotation = Quaternion.Euler(0.0f, head.rotation.eulerAngles.y, 0.0f);
        if (debugMode)
        {
            LineDrawer.DrawLine(this.transform.position, leftShoulder.position, lineColor);
            LineDrawer.DrawLine(this.transform.position, rightShoulder.position, lineColor);
        }
    }
}