using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UIRaycaster), typeof(ColliderRaycaster))]
public abstract class BaseVRPointer : MonoBehaviour
{
    [Min(0.0f)] public float defaultDistance = 5.0f;

    [Space, SerializeField] protected bool debugMode = false;
    [SerializeField, ConditionalHide("debugMode", true)] protected Color normalColor = Color.red;
    [SerializeField, ConditionalHide("debugMode", true)] protected Color hitColor = Color.green;
    [SerializeField, ConditionalHide("debugMode", true)] protected PointerInteractionMode interactionMode = PointerInteractionMode.None;
    [SerializeField, ConditionalHide("debugMode", true)] protected GameObject hitObject = null;
    [SerializeField, ConditionalHide("debugMode", true)] protected float hitDistance = -1.0f;
    [SerializeField, ConditionalHide("debugMode", true)] protected Vector3 hitPoint = new Vector3();
    [SerializeField, ConditionalHide("debugMode", true)] protected Vector3 hitNormal = new Vector3();

    protected ColliderRaycaster colliderRaycaster = null;
    protected UIRaycaster uIRaycaster = null;
    protected Ray ray = new Ray();

    // Used to disable the LineRenderer and all the things related to it
    public delegate void OnDisableDelegate();
    public event OnDisableDelegate onDisableEvent;
    public event OnDisableDelegate onEnableEvent;

    public GameObject HitObject { get => hitObject; private set { } }
    public PointerInteractionMode InteractionMode { get => interactionMode; private set { } }
    public Ray PointerRay { get => ray; private set { } }
    public float HitDistance { get => hitDistance; private set { } }
    public Vector3 HitPoint { get => hitPoint; private set { } }
    public Vector3 HitNormal { get => hitNormal; private set { } }
    public Vector3 PointerLocation { get => ray.origin; private set { } }
    public Vector3 PointerDirection { get => ray.direction; private set { } }
    public bool HasHit { get => (HitObject != null); private set { } }

    protected virtual void Awake()
    {
        // Get a reference to raycasters and the input module
        colliderRaycaster = GetComponent<ColliderRaycaster>();
        uIRaycaster = GetComponent<UIRaycaster>();
    }

    protected virtual void Update()
    {
        // Reset hit data
        ClearHitData();

        // Update ray location and orientation
        UpdateRay();

        // Here we calculate which object among both UI and physical objects is the first being hit by the ray
        // We perform a collider raycast
        // If the collider ray hit something that is closer than current hit distance, we check for distance and update hit object
        colliderRaycaster.CastRay(ray, defaultDistance);
        CheckForClosestObject(colliderRaycaster);

        // We perform a UI raycast
        // If the UI ray hit something that is closer than current hit distance, we check for distance and update hit object
        uIRaycaster.CastRay(ray, defaultDistance);
        CheckForClosestObject(uIRaycaster);

        DrawDebugLine(debugMode);
    }

    protected virtual void UpdateRay()
    {
    }

    protected virtual void ClearHitData()
    {
        hitObject = null;
        hitDistance = defaultDistance;
        interactionMode = PointerInteractionMode.None;
    }

    protected void DrawDebugLine(bool debugMode)
    {
        if (debugMode)
        {
            float distance = (HasHit == true) ? HitDistance : defaultDistance;
            Color color = (HasHit == true) ? hitColor : normalColor;
            LineDrawer.DrawLine(ray, distance, color);
        }
    }

    protected void CheckForClosestObject(BaseRaycaster raycaster)
    {
        // If the collider ray hit something that is closer than current hit distance...
        if (raycaster.HitObject != null && raycaster.HitDistance < hitDistance)
        {
            // ... We update the hit data
            hitDistance = raycaster.HitDistance;
            hitPoint = raycaster.HitLocation;
            hitNormal = raycaster.HitNormal;
            hitObject = raycaster.HitObject;
            interactionMode = raycaster.InteractionMode;
        }
    }

    protected void OnEnable() => onEnableEvent?.Invoke();
    protected void OnDisable() => onDisableEvent?.Invoke();
}