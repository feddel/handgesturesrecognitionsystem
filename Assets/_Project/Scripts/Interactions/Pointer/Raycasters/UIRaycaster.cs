using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIRaycaster : BaseRaycaster
{
    private UIRaycaster()
    {
        componentDescription = "Perform a UI raycast and return the first UI element hit. It required the HandInputModule attached to the EventSystem in order to work.";
    }

    private List<RaycastResult> raycastResultsCache = new List<RaycastResult>();
    private OVRPointerEventData eventData = null;

    public RaycastResult Result
    { get => EventData.pointerCurrentRaycast; private set { } }
    public PointerEventData EventData
    { get => eventData; private set { } }
    public override GameObject HitObject
    { get => Result.gameObject; protected set { } }
    public override Vector3 HitLocation
    { get => Result.worldPosition; protected set { } }
    public override Vector3 HitNormal
    { get => Result.worldNormal; protected set { } }
    public override float HitDistance
    { get => Result.distance; protected set { } }

    private void Awake()
    {
        interactionMode = PointerInteractionMode.UI;
    }

    private void Start()
    {
        // Cache a PointerEventData to avoid calling new every frame
        eventData = new OVRPointerEventData(EventSystem.current);
    }

    public override GameObject CastRay(Ray ray, float distance)
    {
        ClearData();

        // If ray is disabled, return null
        if (isRayEnabled == false) return null;

        // Update camera location and orientation and find the on screen location to determine the direction for the UI ray
        eventData.worldSpaceRay = ray;

        // Through the EventSystem we cast a ray compatible with UI elements and find the first valid result
        EventSystem.current.RaycastAll(eventData, raycastResultsCache);
        eventData.pointerCurrentRaycast = FindFirstRaycast(raycastResultsCache, distance);

        // Source: https://github.com/sirerr/projectlily/blob/master/unitycore/projectlily/Assets/Imports/Scripts/OVRInputModule.cs#L611
        OVRRaycaster ovrRaycaster = eventData.pointerCurrentRaycast.module as OVRRaycaster;
        // We're only interested in intersections from OVRRaycasters
        if (ovrRaycaster)
        {
            // The Unity UI system expects event data to have a screen position
            // so even though this raycast came from a world space ray we must get a screen
            // space position for the camera attached to this raycaster for compatability
            eventData.position = ovrRaycaster.GetScreenPosition(eventData.pointerCurrentRaycast);

            // Find the world position and normal the Graphic the ray intersected
            RectTransform graphicRect = eventData.pointerCurrentRaycast.gameObject.GetComponent<RectTransform>();
            if (graphicRect != null)
            {
                // Set are gaze indicator with this world position and normal
                Vector3 worldPos = eventData.pointerCurrentRaycast.worldPosition;
                Vector3 normal = GetRectTransformNormal(graphicRect);

                RaycastResult temp = eventData.pointerCurrentRaycast;
                temp.worldPosition = worldPos;
                temp.worldNormal = normal;

                eventData.pointerCurrentRaycast = temp;
            }
        }

        // Return hit object
        return HitObject;
    }

    public override void ClearData()
    {
        Result = default;
        if (eventData != null)
            eventData.Reset();
    }

    // Modified version of Unity FindFirstRaycast from BaseInputModule.cs
    // https://github.com/liuqiaosz/Unity/blob/master/UGUI%E6%BA%90%E4%BB%A3%E7%A0%81/UnityEngine.UI/EventSystem/InputModules/BaseInputModule.cs#L41
    private RaycastResult FindFirstRaycast(List<RaycastResult> raycastResults, float distace)
    {
        RaycastResult result = default;
        for (var i = 0; i < raycastResults.Count; ++i)
        {
            // We simply add a distance check to eliminate results located further than the ray range
            if (raycastResults[i].gameObject != null || raycastResults[i].distance < distace)
            {
                result = raycastResults[i];
                break;
            }
        }
        raycastResults.Clear();
        return result;
    }

    // Source: https://github.com/sirerr/projectlily/blob/72df79739ae4c038c0b00cca47888e28efb0e8eb/unitycore/projectlily/Assets/Imports/Scripts/OVRInputModule.cs#L570
    private Vector3 GetRectTransformNormal(RectTransform rectTransform)
    {
        Vector3[] corners = new Vector3[4];
        rectTransform.GetWorldCorners(corners);
        Vector3 BottomEdge = corners[3] - corners[0];
        Vector3 LeftEdge = corners[1] - corners[0];
        rectTransform.GetWorldCorners(corners);
        return Vector3.Cross(BottomEdge, LeftEdge).normalized;
    }
}