using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RaycastingMode
{
    LineTracing,
    SphereTracing,
}

public class ColliderRaycaster : BaseRaycaster
{
    private ColliderRaycaster()
    {
        componentDescription = "Perform a physics raycast and return the first collider hit. " +
            "Raycasting mode allow for two types of cast: linear, which is the most straightforward and Sphere, which allow to have more precision when pointing to far away objects.";
    }
    private bool IsSphereTracingMode { get => (raycastingMode == RaycastingMode.SphereTracing); }

    [SerializeField] private RaycastingMode raycastingMode = RaycastingMode.LineTracing;
    [SerializeField, ConditionalHide("IsSphereTracingMode", true)] private float radius = 0.3f;
    [SerializeField, ConditionalHide("IsSphereTracingMode", true)] private float precision = 0.9965f;
    [SerializeField, ConditionalHide("IsSphereTracingMode", true)] private float precisionDistanceOffset = 0.02f;
    [SerializeField, ConditionalHide("IsSphereTracingMode", true)] private float sphereStartOffset = 1.25f;

    public RaycastHit Hit { get; private set; }
    public override GameObject HitObject { get => Hit.collider?.gameObject; protected set { } }
    public override Vector3 HitLocation { get => Hit.point; protected set { } }
    public override Vector3 HitNormal { get => Hit.normal; protected set { } }
    public override float HitDistance { get => Hit.distance; protected set { } }

    private void Awake()
    {
        interactionMode = PointerInteractionMode.World;
    }

    public override GameObject CastRay(Ray ray, float distance)
    {
        ClearData();

        // If ray is disabled, return null
        if (isRayEnabled == false) return null;

        RaycastHit hit = default;
        switch (raycastingMode)
        {
            case RaycastingMode.LineTracing:
                // Perform a line tracing
                LineDrawer.LineTracing(ray, distance, out hit, false);
                break;

            case RaycastingMode.SphereTracing:
                // Perform a sphere tracing

                Ray tempRay = ray;
                tempRay.origin = ray.origin - ray.direction * radius * sphereStartOffset;
                RaycastHit[] raycastHits = Physics.SphereCastAll(tempRay, radius, distance);
                float closestDot = 0;
                float currentDistance = Mathf.Infinity;
                foreach (RaycastHit raycastHit in raycastHits)
                {
                    Vector3 direction = (raycastHit.point - this.transform.position).normalized;
                    float dot = Vector3.Dot(direction, tempRay.direction);
                    float tempPrecision = precision - (precisionDistanceOffset * (raycastHit.distance / distance));
                    if (dot > tempPrecision && dot > closestDot && raycastHit.distance < currentDistance)
                    {
                        closestDot = dot;
                        currentDistance = raycastHit.distance;
                        hit = raycastHit;
                    }
                }
                break;

            default:
                break;
        }

        // Store the result of the line tracing
        Hit = hit;

        // Return hit object
        return HitObject;
    }

    public override void ClearData() => Hit = default;
}