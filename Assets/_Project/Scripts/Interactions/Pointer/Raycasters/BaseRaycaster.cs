using UnityEngine;

public enum PointerInteractionMode
{ None, World, UI, }

public abstract class BaseRaycaster : MonoBehaviour
{
    [TextArea(3, 10), SerializeField] protected string componentDescription = "";
    [SerializeField] protected bool isRayEnabled = true;

    protected PointerInteractionMode interactionMode = PointerInteractionMode.None;

    public PointerInteractionMode InteractionMode { get => interactionMode; }
    public bool IsHitting { get => (HitObject != null); }
    public abstract GameObject HitObject { get; protected set; }
    public abstract Vector3 HitLocation { get; protected set; }
    public abstract Vector3 HitNormal { get; protected set; }
    public abstract float HitDistance { get; protected set; }
    public abstract GameObject CastRay(Ray ray, float distance);
    public abstract void ClearData();
}