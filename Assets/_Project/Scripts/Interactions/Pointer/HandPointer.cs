using UnityEngine;
using UnityEditor;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class HandPointer : BaseVRPointer
{
    private bool showShoulderData => (pointerOrientationMethod == PointerOrientationMethod.Shoulder); // Used to show and hide shoulder details

    public enum PointerOrientationMethod
    {
        OculusDefault,
        Shoulder,
        Hand,
    }

    [SerializeField] private OVRHand hand = null;
    [SerializeField] private OVRInput.Button clickInput = OVRInput.Button.None;
    [SerializeField] private PointerOrientationMethod pointerOrientationMethod = PointerOrientationMethod.OculusDefault;
    [SerializeField, ConditionalHide("showShoulderData", true)] private Transform shoulder = null;
    [SerializeField, ConditionalHide("showShoulderData", true)] private float shoulderOffset = 0.1f;

    private HandsInputModule inputModule = null;

    public OVRHand Hand { get => hand; private set { } }

    protected override void Awake()
    {
        base.Awake();
        // Get a reference to the input module
        inputModule = FindObjectOfType<HandsInputModule>();

        // This a weird thing: for the system, left pinch is button Three and right pinch is button One
        // Here we determine which one we need to assign to this pointer based on the hand skeleton type
        clickInput = (hand.GetComponent<OVRSkeleton>().GetSkeletonType() == OVRSkeleton.SkeletonType.HandLeft) ? OVRInput.Button.Three : OVRInput.Button.One;
    }

    protected override void Update()
    {
        base.Update();

        // We want the Input Module to generate events if the right input is performed
        inputModule.ProcessPointer(uIRaycaster.EventData, hitObject, clickInput);
    }

    protected override void UpdateRay()
    {
        switch (pointerOrientationMethod)
        {
            // Use the OVR Pointer Pose
            case PointerOrientationMethod.OculusDefault:
                ray.origin = hand.PointerPose.position;
                ray.direction = hand.PointerPose.forward;
                break;

            // Use the custom shoulders
            case PointerOrientationMethod.Shoulder:
                ray.direction = MathUtility.Direction(shoulder.position, hand.transform.position);
                ray.origin = hand.transform.position + ray.direction * shoulderOffset;
                // TODO: Explore how to use Hand Poise Root -OR- Recreate shoulder pointer
                break;

            // Use the hand transform
            case PointerOrientationMethod.Hand:
                ray.origin = hand.transform.position;
                ray.direction = (hand.GetComponent<OVRSkeleton>().GetSkeletonType() == OVRSkeleton.SkeletonType.HandRight) ? -hand.transform.right : hand.transform.right;
                break;

            default:
                break;
        }
    }
}