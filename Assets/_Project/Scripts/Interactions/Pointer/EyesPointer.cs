using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyesPointer : BaseVRPointer
{
    [SerializeField] private Camera headCamera;

    protected override void Awake()
    {
        base.Awake();
        headCamera = (headCamera == null) ? GetComponent<Camera>() : headCamera;
    }

    protected override void UpdateRay()
    {
        ray.origin = headCamera.transform.position;
        ray.direction = headCamera.transform.forward;
    }
}