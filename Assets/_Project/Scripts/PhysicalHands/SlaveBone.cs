﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ConfigurableJoint))]
public class SlaveBone : MonoBehaviour
{
    public Transform target;

    [SerializeField] private bool followTargetPosition = false;
    [SerializeField] private bool followTargetRotation = false;

    private ConfigurableJoint joint;
    private Vector3 xAxis, zAxis, yAxis;
    private Quaternion q;
    private Quaternion initialGlobalRotation;
    private Quaternion initialLocalRotation;
    private Quaternion resultRotation;

    private void Start()
    {
        if (joint == false)
            joint = GetComponent<ConfigurableJoint>();

        xAxis = joint.axis;
        zAxis = Vector3.Cross(joint.axis, joint.secondaryAxis).normalized;
        yAxis = Vector3.Cross(zAxis, xAxis).normalized;

        // Joint frame rotation
        q = Quaternion.LookRotation(zAxis, yAxis);

        if (joint.connectedBody != null && joint.configuredInWorldSpace == false)
            initialLocalRotation = joint.connectedBody.transform.localRotation;
        else
            initialGlobalRotation = joint.transform.rotation;
    }

    public void FixedTick()
    {
        if (followTargetPosition && target)
        {
            // Local
            if (joint.connectedBody != null && !joint.configuredInWorldSpace)
            {
                // Phalanges don't follow position
            }
            // World
            else
            {
                // We should be able to move connectedAnchor in space
                if (joint.autoConfigureConnectedAnchor == true)
                    joint.autoConfigureConnectedAnchor = false;

                // Joint will move the object towards connectedAnchor
                if (joint.targetPosition != Vector3.zero)
                    joint.targetPosition = Vector3.zero;

                // Update the center of the joint frame
                joint.connectedAnchor = target.position;
            }
        }

        if (followTargetRotation == true && target == true)
        {
            // Local
            if (joint.connectedBody != null && joint.configuredInWorldSpace == false)
            {
                // Target local rotation relative to the joint frame + initial offset (local)
                resultRotation = Quaternion.Inverse(q);
                resultRotation *= Quaternion.Inverse(target.localRotation);
                resultRotation *= initialLocalRotation;
                resultRotation *= q;

                joint.targetRotation = resultRotation;
            }
            // World
            else
            {
                // Target world rotation relative to joint frame + initial offset (global)
                resultRotation = Quaternion.Inverse(q);
                resultRotation *= initialGlobalRotation;
                resultRotation *= Quaternion.Inverse(target.rotation);
                resultRotation *= q;

                joint.targetRotation = resultRotation;
            }
        }
    }

    public void SetLocation(Vector3 location)
    {
        this.transform.position = location;
        if (joint != null)
        {
            joint.targetVelocity = Vector3.zero;
            joint.targetAngularVelocity = Vector3.zero;
        }
    }
}