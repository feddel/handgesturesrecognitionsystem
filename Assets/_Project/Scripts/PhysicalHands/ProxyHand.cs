﻿using System;
using UnityEngine;

public class ProxyHand : MonoBehaviour
{
    [Serializable]
    public class RigMap
    {
        public Transform master;
        public SlaveBone slave;
    }

    [SerializeField] private float resetDistance = 0.16f;
    [SerializeField] private OVRSkeleton dataProvider;
    [SerializeField] private Collider palmCollider;
    [SerializeField] private RigMap[] rigMaps = new RigMap[24];

    private Collider[] colliders;

    private void OnEnable()
    {
        // Custom Action to facilitate the initialization process of object that use OVRSkeleton [Federico]
        dataProvider.onInitialize += TeleportHand;
    }

    private void OnDisable()
    {
        // Custom Action to facilitate the initialization process of object that use OVRSkeleton [Federico]
        dataProvider.onInitialize -= TeleportHand;
    }

    private void Awake()
    {
        this.transform.parent = null;

        for (int i = 0; i < rigMaps.Length; i++)
        {
            // Set slave targets
            if (rigMaps[i].master && rigMaps[i].slave)
                rigMaps[i].slave.target = rigMaps[i].master;
        }

        // Ignore phalanges-palm collisions
        colliders = rigMaps[0].slave.GetComponentsInChildren<Collider>();
        for (int i = 0; i < colliders.Length; i++)
        {
            Physics.IgnoreCollision(palmCollider, colliders[i], true);
        }
    }

    private void Update()
    {
        if (dataProvider.Bones.Count != rigMaps.Length)
            return;

        // Update wrist position and rotation in world space
        rigMaps[0].master.position = dataProvider.Bones[0].Transform.position;
        rigMaps[0].master.rotation = dataProvider.Bones[0].Transform.rotation;

        // Update phalanges rotations in local space
        for (int i = 1; i < dataProvider.Bones.Count; i++)
            rigMaps[i].master.localRotation = dataProvider.Bones[i].Transform.localRotation;
    }

    private void FixedUpdate()
    {
        OVRHand hand = HandsManager.Instance.GetHand(dataProvider.GetSkeletonType());
        if (hand != null && IsHandTrackedProperly(hand))
        {
            for (int i = 0; i < rigMaps.Length; ++i)
                rigMaps[i].slave?.FixedTick();

            if (ShouldTeleport())
                TeleportHand();
        }
    }

    private bool IsHandTrackedProperly(OVRHand hand)
    {
        return (hand.HandConfidence == OVRHand.TrackingConfidence.High && hand.IsDataHighConfidence == true && hand.IsTracked);
    }

    private bool ShouldTeleport()
    {
        return (UtilityMath.SquaredDistance(rigMaps[0].slave.transform.position, rigMaps[0].master.position) >= resetDistance);
    }

    public void TeleportHand()
    {
        foreach (RigMap rigMap in rigMaps)
        {
            if (rigMap != null && rigMap.slave != null && rigMap.master != null)
            {
                rigMap.slave.transform.position = rigMap.master.position;
                rigMap.slave.transform.rotation = rigMap.master.rotation;
            }
        }
    }
}