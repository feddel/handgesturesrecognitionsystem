using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class TouchTrigger : MonoBehaviour
{
    [SerializeField] protected UnityEvent triggerEvent = null;

    protected virtual void Awake()
    {
        this.GetComponent<Rigidbody>().useGravity = false;
        this.GetComponent<Collider>().isTrigger = true;
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        triggerEvent?.Invoke();
    }
}