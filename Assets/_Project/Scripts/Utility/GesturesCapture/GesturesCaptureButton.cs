using UnityEngine;
using UnityEngine.Events;

[ExecuteInEditMode]
public class GesturesCaptureButton : MonoBehaviour
{
    private bool _captureMode { get => captureMode == CaptureMode.Update; }

    private enum CaptureMode
    {
        Create,
        Update,
    }

    [SerializeField] private HandBonesData handBonesData = null;
    [SerializeField] private CaptureMode captureMode;
    [Header("Create Mode")]
    [SerializeField] private string path = "Assets/_Project/Resources/Gestures/";
    [Header("Update Mode")]
    [SerializeField] private HandStruct targetHandStruct;
    [Header("Events")]
    [SerializeField] private UnityEvent onTouch;

    private TextMesh handStructNameText = null;
    private const string DEFAULT_NAME = "NewHandStruct";

    private void Update()
    {
        // We lazy load the TextMesh component
        if (handStructNameText == null)
        {
            handStructNameText = GetComponentInChildren<TextMesh>();
        }
        else
        {
            if (captureMode == CaptureMode.Update)
            {
                // If we are in Update mode we show the name of the object we are updating
                string target = (targetHandStruct != null) ? targetHandStruct.HandStructName : "Null";
                handStructNameText.text = $"Updating: {target}";
            }
            else
            {
                handStructNameText.text = "Creating";
                targetHandStruct = null;
            }
        }
    }

    //private void OnValidate()
    //{
    //    // We lazy load the TextMesh component
    //    if (handStructNameText == null)
    //    {
    //        handStructNameText = GetComponentInChildren<TextMesh>();
    //    }
    //    else
    //    {
    //        if (captureMode == CaptureMode.Update)
    //        {
    //            // If we are in Update mode we show the name of the object we are updating
    //            string target = (targetHandStruct != null) ? targetHandStruct.HandStructName : "Null";
    //            handStructNameText.text = $"Updating: {target}";
    //        }
    //        else
    //        {
    //            handStructNameText.text = "Creating";
    //            targetHandStruct = null;
    //        }
    //    }
    //}

    private void Awake()
    {
        handStructNameText = GetComponentInChildren<TextMesh>();
    }

    // If we touch the button, we want to invoke the event associated
    private void OnTriggerEnter(Collider collision) => onTouch?.Invoke();

    /// <summary>
    ///
    /// </summary>
    /// <param name="handStructName"></param>
    public virtual void SaveHandStructScriptableObject(string handStructName)
    {
#if UNITY_EDITOR

        if (captureMode == CaptureMode.Create)
        {
            // We create and initialize a new HandStruct with the data in HandBonesData
            HandStruct handStructObject = CreateHandStructScriptableObject();

            // We check if the file already exist
            string fileName = handStructName + ".asset";
            string fullPath = path + fileName;
            bool exists = System.IO.File.Exists(fullPath);

            // Delete if asset already exists
            if (exists)
                UnityEditor.AssetDatabase.DeleteAsset(fullPath);

            // We create the asset and update the editor
            UnityEditor.AssetDatabase.CreateAsset(handStructObject, fullPath);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh(UnityEditor.ImportAssetOptions.ForceUpdate);
            UnityEditor.EditorUtility.SetDirty(handStructObject);
            Debug.Log("Created new Hand Pose : " + handStructName);
        }
        else if (captureMode == CaptureMode.Update && targetHandStruct != null)
        {
            // We copy the data in HandBonesData into the target HandStruct
            HandStruct tempHandStruct = targetHandStruct;
            tempHandStruct.Initialize(handBonesData, targetHandStruct.HandStructName);
            targetHandStruct = tempHandStruct;
            handStructNameText.text = $"{targetHandStruct.HandStructName} updated!";
            targetHandStruct = null;
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh(UnityEditor.ImportAssetOptions.ForceUpdate);
            Debug.Log("Updated Hand Pose : " + handStructName);
        }
#else
    Debug.Log("Scriptable Objects can only be saved from within the Unity Editor. Consider storing in another format like JSON instead.");
#endif
    }

    public virtual void CreateUniqueHandStruct(string handStructName = null)
    {
        if (handBonesData == null) return;

        // Don't allow empty pose names
        if (string.IsNullOrEmpty(handStructName))
            handStructName = DEFAULT_NAME;

        string formattedName = handStructName;
        string fullPath = path + formattedName + ".asset";
        bool exists = System.IO.File.Exists(fullPath);
        int checkCount = 0;
        // Find a path that doesn't exist
        while (exists)
        {
            // We add a number if we find a file with the same name
            formattedName = handStructName + " " + checkCount;
            exists = System.IO.File.Exists(path + formattedName + ".asset");
            checkCount++;
        }
        // Save the new pose
        SaveHandStructScriptableObject(formattedName);
    }

    public virtual HandStruct CreateHandStructScriptableObject()
    {
#if UNITY_EDITOR
        // We create a new instance of an HandStruct and we initialize it with the values from the HandBonesData
        HandStruct handStructObject = UnityEditor.Editor.CreateInstance<HandStruct>();
        handStructObject.Initialize(handBonesData);
        return handStructObject;
#else
    return null;
#endif
    }
}