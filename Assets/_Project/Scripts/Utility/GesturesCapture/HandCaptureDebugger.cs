using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandCaptureDebugger : MonoBehaviour
{
    private HandBonesData[] handBonesDatas = new HandBonesData[0];
    [SerializeField] private float scale = 0.08f;

    private void Start()
    {
        handBonesDatas = FindObjectsOfType<HandBonesData>();
    }

    private void Update()
    {
        foreach (HandBonesData handBonesData in handBonesDatas)
        {
            LineDrawer.DrawLine(handBonesData.gameObject.transform.position, handBonesData.gameObject.transform.position + handBonesData.WristJoint.transform.up * scale, Color.green);
            LineDrawer.DrawLine(handBonesData.gameObject.transform.position, handBonesData.gameObject.transform.position + handBonesData.WristJoint.transform.right * scale, Color.red);
            LineDrawer.DrawLine(handBonesData.gameObject.transform.position, handBonesData.gameObject.transform.position + handBonesData.WristJoint.transform.forward * scale, Color.blue);
        }
    }
}