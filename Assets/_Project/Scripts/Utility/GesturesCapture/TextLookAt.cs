using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextLookAt : MonoBehaviour
{
    [SerializeField] private Transform target;

    private void Awake()
    {
        if (target == null)
        {
            Debug.LogWarning("No target selected to look at.", this);
        }
    }

    private void Update()
    {
        if (target == null) return;

        Vector3 direction = (this.transform.position - target.position).normalized;
        Quaternion rotation = Quaternion.LookRotation(direction);
        this.transform.rotation = rotation;
    }
}