using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathUtility
{
    public static Vector3 RotateAround(this Vector3 vector, Vector3 origin, Vector3 axis, float angle)
    {
        return origin + Quaternion.AngleAxis(angle, axis) * (vector - origin);
    }

    public static Vector3 Rotate(this Vector3 vector, Vector3 axis, float angle)
    {
        return Quaternion.AngleAxis(angle, axis) * vector;
    }

    public static Vector3 Direction(this Vector3 from, Vector3 to)
    {
        return (to - from).normalized;
    }
}

public class GizmosUtility : MonoBehaviour
{
    // Arbitrary value that I found to be big enough to draw an "infinite" ray, but not too much to be considered above the engine limit
    public static readonly float MAX_DISTANCE = 3.402823e+18f;

    private static Vector3 otherOne = new Vector3(0.5f, -2.0f, 3.5f);

    /// <summary>
    /// Allows to easily pass a Vector3 to form a point to the GL API
    /// </summary>
    /// <param name="position"></param>
    private static void Point(Vector3 position)
    {
        GL.Vertex3(position.x, position.y, position.z);
    }

    /// <summary>
    /// Allows to easily pass a pair of Vector3 to form a line to the GL API
    /// </summary>
    /// <param name="position"></param>
    private static void Line(Vector3 start, Vector3 end)
    {
        Point(start);
        Point(end);
    }

    /// <summary>
    /// Use to draw a line that goes from Points[0]...Points[n] using GL API. Run this in OnDrawGizmos() or OnPostRender().
    /// </summary>
    /// <param name="points">Starting point.</param>
    /// <param name="lineMat">Material applied to the line.</param>
    /// <param name="color">Color applied to the line.</param>
    public static void GLDrawLine(List<Vector3> points, Material material, Color color)
    {
        GLDrawLine(points.ToArray(), material, color);
    }

    /// <summary>
    /// Use to draw a line that goes from Points[0]...Points[n] using GL API. Run this in OnDrawGizmos() or OnPostRender().
    /// </summary>
    /// <param name="points">Starting point.</param>
    /// <param name="lineMat">Material applied to the line.</param>
    /// <param name="color">Color applied to the line.</param>
    public static void GLDrawLine(Vector3[] points, Material material, Color color)
    {
        GL.Begin(GL.LINES);
        material.SetPass(0);
        GL.Color(color);

        for (int i = 0; i < points.Length; ++i)
            GL.Vertex3(points[i].x, points[i].y, points[i].z);

        GL.End();
    }

    /// <summary>
    /// Use to draw a line that goes from Start to End using GL API. Run this in OnDrawGizmos() or OnPostRender().
    /// </summary>
    /// <param name="start">Starting point.</param>
    /// <param name="end">End point.</param>
    /// <param name="lineMat">Material applied to the line.</param>
    /// <param name="color">Color applied to the line.</param>
    public static void GLDrawLine(Vector3 start, Vector3 end, Material material, Color color)
    {
        GL.Begin(GL.LINES);
        material.SetPass(0);
        GL.Color(color);
        Line(start, end);
        GL.End();
    }

    /// <summary>
    /// Use to draw a ray that goes infintely from Origin toward Direction using GL API. Run this in OnDrawGizmos() or OnPostRender().
    /// </summary>
    /// <param name="start">Starting point.</param>
    /// <param name="end">End point.</param>
    /// <param name="lineMat">Material applied to the line.</param>
    /// <param name="color">Color applied to the line.</param>
    public static void GLDrawRay(Ray ray, Material material, Color color)
    {
        GLDrawLine(ray.origin, ray.origin + ray.direction * MAX_DISTANCE, material, color);
    }

    /// <summary>
    /// Use to draw a ray that goes infintely from Origin toward Direction using GL API. Run this in OnDrawGizmos() or OnPostRender().
    /// </summary>
    /// <param name="start">Starting point.</param>
    /// <param name="end">End point.</param>
    /// <param name="lineMat">Material applied to the line.</param>
    /// <param name="color">Color applied to the line.</param>
    public static void GLDrawRay(Vector3 origin, Vector3 direction, Material material, Color color)
    {
        GLDrawLine(origin, origin + direction * MAX_DISTANCE, material, color);
    }

    public static void GLDrawCircle(Vector3 center, Vector3 axis, float radius, Material material, Color color, int precision = 100)
    {
        GL.Begin(GL.LINES);
        material.SetPass(0);
        GL.Color(color);

        float angle = 360f / (float)precision;
        Vector3 v = Vector3.Cross(axis, (Vector3.Dot(axis, Vector3.one) == 0.0f ? otherOne : Vector3.one)).normalized * radius;
        for (int i = 0; i < precision; ++i)
        {
            Vector3 t = Quaternion.AngleAxis(angle, axis) * v;
            Line(center + v, center + t);
            v = t;
        }

        GL.End();
    }

    // Source: https://github.com/ratkingsminion/game-jam-base-code/blob/master/Unity3D/Base/Scripts/Helpers/GizmoExtras.cs (Line: 33)
    public static void GLDrawCapsule(Vector3 center, Vector3 axis, float radius, float height, Material material, Color color, int precision = 6)
    {
        GL.Begin(GL.LINES);
        material.SetPass(0);
        GL.Color(color);

        var innerHeight = height - radius * 2f;
        var doublePrecision = precision * 2;
        float angle = 360f / (float)doublePrecision;
        Vector3 v = Vector3.Cross(axis, (Vector3.Dot(axis, Vector3.one) == 0.0f ? otherOne : Vector3.one)).normalized * radius;
        var top = center + axis * (innerHeight * 0.5f);
        var bottom = center - axis * (innerHeight * 0.5f);
        for (int i = 0; i < doublePrecision; ++i)
        {
            Vector3 t = MathUtility.Rotate(v, axis, angle);
            Line(bottom + v, bottom + t);
            Line(top + v, top + t);
            Line(top + v, bottom + v);
            if (i <= precision)
            {
                // spheres
                Vector3 sphereTop = top + t;
                Vector3 sphereBottom = bottom + t;
                Vector3 a = Vector3.Cross(t, axis);
                for (int j = 0; j < precision; ++j)
                {
                    var stn = MathUtility.RotateAround(sphereTop, top, a, angle);
                    var sbn = MathUtility.RotateAround(sphereBottom, bottom, -a, angle);
                    Line(sphereTop, stn);
                    Line(sphereBottom, sbn);
                    sphereTop = stn;
                    sphereBottom = sbn;
                }
            }
            v = t;
        }

        GL.End();
    }
}