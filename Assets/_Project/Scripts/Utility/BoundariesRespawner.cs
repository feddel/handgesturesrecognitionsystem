using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class BoundariesRespawner : MonoBehaviour
{
    private enum BoundariesType
    {
        Absolute,
        Relative,
    }

    [SerializeField] private Pose originalPose;
    [Space]
    [SerializeField] private BoundariesType boundariesType = BoundariesType.Absolute;
    [SerializeField] private bool enableMinBoundaries = true;
    [DraggablePoint] public Vector3 minBoundaries;
    [SerializeField] private bool enableMaxBoundaries = false;
    [DraggablePoint] public Vector3 maxBoundaries;
    [Space]
    [SerializeField] private bool enableX = false;
    [SerializeField] private bool enableY = true;
    [SerializeField] private bool enableZ = false;
    [Header("Debug")]
    [SerializeField] private bool debugMode = false;
    [SerializeField] private Color color;

    private Rigidbody objectRigidbody;

    private void Start()
    {
        CheckForValidity();
    }

    private void OnEnable()
    {
        CheckForValidity();
    }

    private void Update()
    {
        if (debugMode)
            DrawBoundaries();
    }

    private void FixedUpdate()
    {
        if (ShouldRespawn() == true)
            Respawn();
    }

    private void Respawn()
    {
        this.transform.position = originalPose.position;
        this.transform.rotation = originalPose.rotation;

        if (objectRigidbody != null)
        {
            objectRigidbody.velocity = Vector3.zero;
            objectRigidbody.angularVelocity = Vector3.zero;
        }

        if (debugMode)
            Debug.LogWarning($"{this.gameObject.name} was out of boundaries and has been respawned.", this);
    }

    private bool ShouldRespawn()
    {
        if (enableX == true)
        {
            if ((enableMinBoundaries && this.transform.position.x < minBoundaries.x) ||
                (enableMaxBoundaries && this.transform.position.x > maxBoundaries.x))
                return true;
        }
        if (enableY == true)
        {
            if ((enableMinBoundaries && this.transform.position.y < minBoundaries.y) ||
                (enableMaxBoundaries && this.transform.position.y > maxBoundaries.y))
                return true;
        }
        if (enableZ == true)
        {
            if ((enableMinBoundaries && this.transform.position.z < minBoundaries.z) ||
                (enableMaxBoundaries && this.transform.position.z > maxBoundaries.z))
                return true;
        }
        return false;
    }

    private void CheckForValidity()
    {
        if (boundariesType == BoundariesType.Relative && ShouldRespawn() == true)
        {
            Debug.LogWarning("The original position of the target object of BoundariesRespawner is set outside the boundaries. BoundariesRespawner has been disabled.", this);
            this.enabled = false;
        }
    }

    [ContextMenu("SetOriginalLocationAndOrientation")]
    private void SetOriginalLocationAndOrientation()
    {
        originalPose = new Pose(this.transform.position, this.transform.rotation);
    }

    private void OnValidate()
    {
        if (maxBoundaries.x < minBoundaries.x)
        {
            minBoundaries.x = maxBoundaries.x;
        }
        else if (minBoundaries.x > maxBoundaries.x)
        {
            maxBoundaries.x = minBoundaries.x;
        }

        if (maxBoundaries.y < minBoundaries.y)
        {
            minBoundaries.y = maxBoundaries.y;
        }
        else if (minBoundaries.y > maxBoundaries.y)
        {
            maxBoundaries.y = minBoundaries.y;
        }

        if (maxBoundaries.z < minBoundaries.z)
        {
            minBoundaries.z = maxBoundaries.z;
        }
        else if (minBoundaries.z > maxBoundaries.z)
        {
            maxBoundaries.z = minBoundaries.z;
        }
    }

    private void DrawBoundaries()
    {
        LineDrawer.DrawAxisAlignedBox(minBoundaries, maxBoundaries, color);
    }
}