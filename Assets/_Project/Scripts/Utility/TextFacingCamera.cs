using UnityEngine;

/// <summary>
/// Debug tool meant to show which gesture is being recognized to the associated HandGestureStateMachine 
/// </summary>
/// 
[RequireComponent(typeof(MeshRenderer), typeof(TextMesh))]
public class TextFacingCamera : MonoBehaviour
{
    [SerializeField] private float verticalOffset = 0.1f;
    [SerializeField] private Camera mainCamera = null;
    [SerializeField] private Color textColor = Color.red;

    private HandGestureStateMachine handGestureStateMachine;
    private TextMesh textMesh;

    private void OnEnable()
    {
        if (handGestureStateMachine != null)
            handGestureStateMachine.onEnterEvent += SetHandStateText;
    }

    private void OnDisable()
    {
        if (handGestureStateMachine != null)
            handGestureStateMachine.onEnterEvent -= SetHandStateText;
    }

    public void Initialize()
    {
        textMesh = GetComponent<TextMesh>();
        handGestureStateMachine = this.transform.parent.GetComponent<HandGestureStateMachine>();
        if (handGestureStateMachine != null)
            handGestureStateMachine.onEnterEvent += SetHandStateText;
        else
            Debug.LogWarning("No HandGestureStateMachine found in parent.");

        this.transform.parent = null;
        mainCamera = (mainCamera == null) ? Camera.main : mainCamera;
    }

    private void SetHandStateText(HandState handState)
    {
        textMesh.text = handState.name;
        textMesh.color = textColor;
    }

    private void Update()
    {
        if (handGestureStateMachine == null)
        {
            Debug.LogWarning("Missing HandGestureStateMachine reference.");
            return;
        }

        // We set the position to be the same as the parent but with a vertical offset
        this.transform.position = handGestureStateMachine.transform.position + handGestureStateMachine.transform.up * verticalOffset;

        // We set the local rotation to always been facing the camera
        if (mainCamera)
            this.transform.localRotation = Quaternion.LookRotation(this.transform.position - mainCamera.transform.position);
    }
}