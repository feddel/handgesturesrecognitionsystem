using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A Line Drawer that use GL.Lines to display debug lines. 
/// Debug Lines are normally exclusive to the scene view, but with this we can draw lies in the game view as well.
/// The interface is heavily inspired by UE4 Line Tracing.
/// </summary>

[ExecuteInEditMode]
public class LineDrawer : MonoBehaviour
{
    [Flags]
    private enum ViewMode // Used to determine where to draw lines
    { None, Scene, Game, }

    public struct LineDefinition
    {
        public LineDefinition(Vector3 start, Vector3 end, Material material = null,
            float lifetime = -1.0f, Color color = default)
        {
            this.distance = Vector3.Distance(start, end);
            this.start = start;
            this.end = end;
            this.material = material;
            this.lifetime = lifetime;
            this.color = color;
            this.isExpired = false;
        }

        // By definition, a Ray is infinite, but we have the possibility to
        // define a ray with a specified distance to create a line
        public LineDefinition(Ray ray, float distance, Material material = null,
            float lifetime = -1.0f, Color color = default)
        {
            this.distance = distance;
            this.start = ray.origin;
            this.end = ray.origin + ray.direction * distance;
            this.material = material;
            this.lifetime = lifetime;
            this.color = color;
            this.isExpired = false;
        }

        public LineDefinition(Ray ray, Material material = null,
            float lifetime = Mathf.Infinity, Color color = default)
        {
            // By definition, a Ray is infinite
            this.distance = GizmosUtility.MAX_DISTANCE;
            this.start = ray.origin;
            this.end = ray.origin + ray.direction * this.distance;
            this.material = material;
            this.lifetime = lifetime;
            this.color = color;
            this.isExpired = false;
        }

        public float distance;
        public Vector3 start;
        public Vector3 end;
        public Material material;
        public float lifetime;
        public Color color;
        public bool isExpired;
    }

    // Singleton instance
    private static LineDrawer instance;

    [Header("Default Global Settings")]
    [SerializeField] private ViewMode viewMode = ~ViewMode.None;
    [SerializeField] private Color defaultColor = Color.red;
    [SerializeField] private Color defaultHitColor = Color.green;
    [SerializeField] private Material defaultMaterial;
    private static List<LineDefinition> definitions = new List<LineDefinition>();

    // These variables are there only to allow functions to be called as statics
    // while allowing customization of the global settings in editor
    private static Material sDefaultMaterial;
    private static ViewMode sViewMode = ~ViewMode.None;
    private static Color sDefaultColor = Color.red;
    private static Color sDefaultHitColor = Color.green;

    private void Awake()
    {
        // This ensure the Singleton pattern to work properly
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;

        // Initialize static variables
        UpdateStaticFields();

        // Set a default material if no one is selected
        defaultMaterial = (defaultMaterial == null) ? Resources.Load<Material>("Materials/DrawMaterial") : defaultMaterial;
        if (defaultMaterial == null)
            Debug.LogError($"{this} does not have a valid Default Material stored in Assets/Resources/Materials.", this);

        // This will get rid of all lines, including the ones with infinite lifetime.
        definitions.Clear();

        // Enable gizmos in editor mode, disable them if not
#if UNITY_EDITOR
        UnityEditor.SceneView sceneView = UnityEditor.EditorWindow.GetWindow<UnityEditor.SceneView>();
        sceneView.drawGizmos = true;
#else
        UnityEditor.SceneView sceneView = UnityEditor.EditorWindow.GetWindow<UnityEditor.SceneView>();
        sceneView.drawGizmos = false;
#endif
    }

    private void Update()
    {
        if (Application.isPlaying == false)
        {
            OnPostRender();
            OnDrawGizmos();
        }
    }

    /// <summary>
    /// Add a new Line that goes from Start to End to the LineDrawer lines list.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="enableLine"></param>
    /// <param name="color"></param>
    /// <param name="lifetime"></param>
    /// <param name="material"></param>
    public static void DrawLine(Vector3 start, Vector3 end, Color color = default,
        float lifetime = 0.0f, Material material = null)
    {
        if (sViewMode == ViewMode.None) return;

        material = (material == null) ? sDefaultMaterial : material;
        color = (color == default) ? sDefaultColor : color;
        LineDefinition newLineDefinition = new LineDefinition(start, end, material, lifetime, color);
        definitions.Add(newLineDefinition);
    }

    public static void DrawLine(Ray ray, float distance, Color color = default,
    float lifetime = 0.0f, Material material = null)
    {
        if (sViewMode == ViewMode.None) return;

        material = (material == null) ? sDefaultMaterial : material;
        color = (color == default) ? sDefaultColor : color;
        LineDefinition newLineDefinition = new LineDefinition(ray, distance, material, lifetime, color);
        definitions.Add(newLineDefinition);
    }

    /// <summary>
    /// Add a new Ray to the LineDrawer lines list.
    /// </summary>
    /// <param name="ray"></param>
    /// <param name="enableLine"></param>
    /// <param name="color"></param>
    /// <param name="lifetime"></param>
    /// <param name="material"></param>
    public static void DrawRay(Ray ray, Color color = default, float lifetime = 0.0f, Material material = null)
    {
        if (sViewMode == ViewMode.None) return;

        material = (material == null) ? sDefaultMaterial : material;
        color = (color == default) ? sDefaultColor : color;
        LineDefinition newRayDefinition = new LineDefinition(ray, material, lifetime, color);
        definitions.Add(newRayDefinition);
    }

    /// <summary>
    /// Add a new Ray that goes from Origin and infinitely toward Direction to the LineDrawer lines list.
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="direction"></param>
    /// <param name="enableLine"></param>
    /// <param name="color"></param>
    /// <param name="lifetime"></param>
    /// <param name="material"></param>
    public static void DrawRay(Vector3 origin, Vector3 direction, Color color = default,
        float lifetime = 0.0f, Material material = null)
    {
        if (sViewMode == ViewMode.None) return;

        material = (material == null) ? sDefaultMaterial : material;
        color = (color == default) ? sDefaultColor : color;
        LineDefinition newRayDefinition = new LineDefinition(origin, direction, material, lifetime, color);
        definitions.Add(newRayDefinition);
    }

    /// <summary>
    /// Perform an infinite Raycast that return out a RaycastHit object. It's provided with a way to visualize and customize the ray.
    /// </summary>
    /// <param name="ray"></param>
    /// <param name="hitResult"></param>
    /// <param name="enableLine"></param>
    /// <param name="layers">Layers can be customized with bitwise operations and Layers.NameToLayer.</param>
    /// <param name="queryTrigger">Define if the Ray should ignore Trigger colliders.</param>
    /// <param name="color"></param>
    /// <param name="hitColor"></param>
    /// <param name="lifetime">Define how long the line should be kept alive. 0 mean only one frame, -1 mean forever.</param>
    /// <param name="material"></param>
    /// <returns>Return true if it hit something.</returns>
    public static bool RayTracing(Ray ray, out RaycastHit hitResult, bool enableLine = true, int layers = Physics.AllLayers,
        QueryTriggerInteraction queryTrigger = QueryTriggerInteraction.Ignore, Color color = default,
        Color hitColor = default, float lifetime = 0.0f, Material material = null)
    {
        bool bHit = Physics.Raycast(ray, out hitResult, GizmosUtility.MAX_DISTANCE, layers, queryTrigger);
        if (enableLine == true)
        {
            color = (color == default) ? sDefaultColor : color;
            hitColor = (hitColor == default) ? sDefaultHitColor : hitColor;
            Color finalColor = (bHit == true) ? hitColor : color;
            float distance = (bHit == true) ? hitResult.distance : GizmosUtility.MAX_DISTANCE;
            DrawLine(ray, distance, finalColor, lifetime, material);
        }
        return bHit;
    }

    /// <summary>
    /// Perform an infinite Raycast that return out an array of RaycastHit[] objects. It's provided with a way to visualize and customize the ray.
    /// </summary>
    /// <param name="ray"></param>
    /// <param name="hitResults"></param>
    /// <param name="enableLine"></param>
    /// <param name="layers">Layers can be customized with bitwise operations and Layers.NameToLayer.</param>
    /// <param name="queryTrigger">Define if the Ray should ignore Trigger colliders.</param>
    /// <param name="color"></param>
    /// <param name="hitColor"></param>
    /// <param name="lifetime">Define how long the line should be kept alive. 0 mean only one frame, -1 mean forever.</param>
    /// <param name="material"></param>
    /// <returns>Return true if it hit something.</returns>
    public static bool MultiRayTracing(Ray ray, out RaycastHit[] hitResults, bool enableLine = true,
        int layers = Physics.AllLayers, QueryTriggerInteraction queryTrigger = QueryTriggerInteraction.Ignore,
        Color color = default, Color hitColor = default, float lifetime = 0.0f, Material material = null)
    {
        hitResults = Physics.RaycastAll(ray, GizmosUtility.MAX_DISTANCE, layers, queryTrigger);
        bool bHit = (hitResults.Length > 0);
        if (enableLine == true)
        {
            color = (color == default) ? sDefaultColor : color;
            hitColor = (hitColor == default) ? sDefaultHitColor : hitColor;
            Color finalColor = (bHit) ? hitColor : color;
            DrawRay(ray, finalColor, lifetime, material);
        }
        return bHit;
    }

    /// <summary>
    /// Perform a Raycast from Start to End that return out a RaycastHit object. It's provided with a way to visualize and customize the ray.
    /// </summary>
    /// <param name="ray"></param>
    /// <param name="hitResult"></param>
    /// <param name="enableLine"></param>
    /// <param name="layers">Layers can be customized with bitwise operations and Layers.NameToLayer.</param>
    /// <param name="queryTrigger">Define if the Ray should ignore Trigger colliders.</param>
    /// <param name="color"></param>
    /// <param name="hitColor"></param>
    /// <param name="lifetime">Define how long the line should be kept alive. 0 mean only one frame, -1 mean forever.</param>
    /// <param name="material"></param>
    /// <returns>Return true if it hit something.</returns>
    public static bool LineTracing(Vector3 start, Vector3 end, out RaycastHit hitResult, bool enableLine = true,
        int layers = Physics.AllLayers, QueryTriggerInteraction queryTrigger = QueryTriggerInteraction.Ignore,
        Color color = default, Color hitColor = default, float lifetime = 0.0f, Material material = null)
    {
        float distance = Vector3.Distance(start, end);
        Ray ray = new Ray(start, (end - start).normalized);
        bool bHit = Physics.Raycast(ray, out hitResult, distance, layers, queryTrigger);
        if (enableLine == true)
        {
            color = (color == default) ? sDefaultColor : color;
            hitColor = (hitColor == default) ? sDefaultHitColor : hitColor;
            Color finalColor = (bHit == true) ? hitColor : color;
            distance = (bHit == true) ? hitResult.distance : distance;
            DrawLine(ray, distance, finalColor, lifetime, material);
        }
        return bHit;
    }

    public static bool LineTracing(Ray ray, float distance, out RaycastHit hitResult, bool enableLine = true,
    int layers = Physics.AllLayers, QueryTriggerInteraction queryTrigger = QueryTriggerInteraction.Ignore,
    Color color = default, Color hitColor = default, float lifetime = 0.0f, Material material = null)
    {
        bool bHit = Physics.Raycast(ray, out hitResult, distance, layers, queryTrigger);
        if (enableLine == true)
        {
            color = (color == default) ? sDefaultColor : color;
            hitColor = (hitColor == default) ? sDefaultHitColor : hitColor;
            Color finalColor = (bHit) ? hitColor : color;
            distance = (bHit == true) ? hitResult.distance : distance;
            DrawLine(ray, distance, finalColor, lifetime, material);
        }
        return bHit;
    }

    /// <summary>
    /// Perform a Raycast from Start to End that return out an array of RaycastHit[] objects. It's provided with a way to visualize and customize the ray.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="hitResults"></param>
    /// <param name="enableLine"></param>
    /// <param name="layers">Layers can be customized with bitwise operations and Layers.NameToLayer.</param>
    /// <param name="queryTrigger">Define if the Ray should ignore Trigger colliders.</param>
    /// <param name="color"></param>
    /// <param name="hitColor"></param>
    /// <param name="lifetime">Define how long the line should be kept alive. 0 mean only one frame, -1 mean forever.</param>
    /// <param name="material"></param>
    /// <returns>Return true if it hit something.</returns>
    public static bool MultiLineTracing(Vector3 start, Vector3 end, out RaycastHit[] hitResults, bool enableLine = true,
        int layers = Physics.AllLayers, QueryTriggerInteraction queryTrigger = QueryTriggerInteraction.Ignore,
        Color color = default, Color hitColor = default, float lifetime = 0.0f, Material material = null)
    {
        float distance = Vector3.Distance(start, end);
        Ray ray = new Ray(start, (end - start).normalized);
        hitResults = Physics.RaycastAll(ray, distance, layers, queryTrigger);
        bool bHit = (hitResults.Length > 0);
        if (enableLine == true)
        {
            color = (color == default) ? sDefaultColor : color;
            hitColor = (hitColor == default) ? sDefaultHitColor : hitColor;
            Color finalColor = (bHit) ? hitColor : color;
            DrawLine(ray, distance, finalColor, lifetime, material);
        }
        return bHit;
    }

    public static bool MultiLineTracing(Ray ray, float distance, out RaycastHit[] hitResults, bool enableLine = true,
    int layers = Physics.AllLayers, QueryTriggerInteraction queryTrigger = QueryTriggerInteraction.Ignore,
    Color color = default, Color hitColor = default, float lifetime = 0.0f, Material material = null)
    {
        hitResults = Physics.RaycastAll(ray, distance, layers, queryTrigger);
        bool bHit = (hitResults.Length > 0);
        if (enableLine == true)
        {
            color = (color == default) ? sDefaultColor : color;
            hitColor = (hitColor == default) ? sDefaultHitColor : hitColor;
            Color finalColor = (bHit) ? hitColor : color;
            DrawLine(ray, distance, finalColor, lifetime, material);
        }
        return bHit;
    }

    public static void DrawAxisAlignedBox(Vector3 min, Vector3 max, Color color = default, float lifetime = 0.0f, Material material = null)
    {
        Vector3 diff = max - min;

        Vector3 minLeftDown = min;
        Vector3 minLeftUp = min + new Vector3(0.0f, 0.0f, diff.z);
        Vector3 minRightUp = min + new Vector3(diff.x, 0.0f, diff.z);
        Vector3 minRightDown = min + new Vector3(diff.x, 0.0f, 0.0f);

        Vector3 maxLeftDown = minLeftDown + new Vector3(0.0f, diff.y, 0.0f);
        Vector3 maxLeftUp = minLeftUp + new Vector3(0.0f, diff.y, 0.0f);
        Vector3 maxRightUp = minRightUp + new Vector3(0.0f, diff.y, 0.0f);
        Vector3 maxRightDown = minRightDown + new Vector3(0.0f, diff.y, 0.0f);

        DrawLine(minLeftDown, minLeftUp, color, lifetime, material);
        DrawLine(minLeftUp, minRightUp, color, lifetime, material);
        DrawLine(minRightUp, minRightDown, color, lifetime, material);
        DrawLine(minRightDown, minLeftDown, color, lifetime, material);

        DrawLine(minLeftDown, maxLeftDown, color, lifetime, material);
        DrawLine(minLeftUp, maxLeftUp, color, lifetime, material);
        DrawLine(minRightUp, maxRightUp, color, lifetime, material);
        DrawLine(minRightDown, maxRightDown, color, lifetime, material);

        DrawLine(maxLeftDown, maxLeftUp, color, lifetime, material);
        DrawLine(maxLeftUp, maxRightUp, color, lifetime, material);
        DrawLine(maxRightUp, maxRightDown, color, lifetime, material);
        DrawLine(maxRightDown, maxLeftDown, color, lifetime, material);
    }

    private void UpdateLinesDefinitionList()
    {
        for (int i = 0; i < definitions.Count; ++i)
        {
            LineDefinition definition = definitions[i];
            // Delete expired definitions
            if (definition.isExpired == true)
            {
                definitions.RemoveAt(i);
                i--;
                continue;
            }
            // Mark as expired any line which a expired lifetime
            // LineDefinitions with lifetime of -1.0f will never been removed
            if (definition.lifetime <= 0.0f && definition.lifetime > -1.0f)
            {
                definition.isExpired = true;
            }
            // Update LineDefinition lifetime
            definition.lifetime -= Time.deltaTime;
            definitions[i] = definition;
        }
    }

    private void UpdateStaticFields()
    {
        sViewMode = viewMode;
        sDefaultMaterial = defaultMaterial;
        sDefaultColor = defaultColor;
        sDefaultHitColor = defaultHitColor;
    }

    /// <summary>
    /// Draw in Game view
    /// </summary>
    private void OnPostRender()
    {
        if (viewMode.HasFlag(ViewMode.Game))
        {
            for (int i = 0; i < definitions.Count; ++i)
            {
                LineDefinition definition = definitions[i];
                GizmosUtility.GLDrawLine(definition.start, definition.end, definition.material, definition.color);
            }
        }

        UpdateLinesDefinitionList();
    }

#if UNITY_EDITOR
    /// <summary>
    /// Draw in Scene view
    /// </summary>
    private void OnDrawGizmos()
    {
        if (viewMode.HasFlag(ViewMode.Scene) == false) return;

        for (int i = 0; i < definitions.Count; ++i)
        {
            LineDefinition definition = definitions[i];
            Debug.DrawLine(definition.start, definition.end, definition.color);
        }
    }
#endif

    private void OnValidate()
    {
        UpdateStaticFields();
    }
}