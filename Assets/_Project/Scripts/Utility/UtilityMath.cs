using UnityEngine;

// COPIED FROM OVRCommon.cs
public static class UtilityMath
{
    public static Vector3 FromFlippedXVector3(this Vector3 v)
    {
        return new Vector3() { x = -v.x, y = v.y, z = v.z };
    }

    public static Vector3 FromFlippedZVector3(this Vector3 v)
    {
        return new Vector3() { x = v.x, y = v.y, z = -v.z };
    }

    public static Vector3 ToFlippedXVector3(this Vector3 v)
    {
        return new Vector3() { x = -v.x, y = v.y, z = v.z };
    }

    public static Vector3 ToFlippedZVector3(this Vector3 v)
    {
        return new Vector3() { x = v.x, y = v.y, z = -v.z };
    }

    public static Quaternion FromFlippedXQuaternion(this Quaternion q)
    {
        return new Quaternion() { x = q.x, y = -q.y, z = -q.z, w = q.w };
    }

    public static Quaternion FromFlippedZQuaternion(this Quaternion q)
    {
        return new Quaternion() { x = -q.x, y = -q.y, z = q.z, w = q.w };
    }

    public static Quaternion ToFlippedXQuaternion(this Quaternion q)
    {
        return new Quaternion() { x = q.x, y = -q.y, z = -q.z, w = q.w };
    }

    public static Quaternion ToFlippedZQuaternion(this Quaternion q)
    {
        return new Quaternion() { x = -q.x, y = -q.y, z = q.z, w = q.w };
    }

    // CUSTOM STUFF
    public static bool IsNearlyZero(this float f)
    {
        return (Mathf.Abs(f) < Mathf.Epsilon);
    }

    public static Vector3 Direction(Vector3 from, Vector3 to)
    {
        return (to - from).normalized;
    }

    public static float SquaredDistance(Vector3 from, Vector3 to)
    {
        return (to - from).sqrMagnitude;
    }
}