using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Selectable))]
public class UITransitioner : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    private enum DeltaTimeType
    {
        Default,
        Fixed,
        Unscaled,
    }

    [SerializeField] private bool debugMode = false;
    [SerializeField] private Image image = null;
    [SerializeField] private Color32 defaultColor = Color.grey;
    [SerializeField] private Color32 hoverColor = Color.white;
    [SerializeField] private Color32 downColor = Color.green;
    [SerializeField, Min(0.0f)] private float fadeDuration = 0.1f;
    [SerializeField] private DeltaTimeType deltaTimeType = DeltaTimeType.Default;

    private Selectable selectable = null;

    public UnityEvent onEnter;
    public UnityEvent onExit;
    public UnityEvent onDown;
    public UnityEvent onUp;

    private void Awake()
    {
        // We disable the default transition logic
        selectable = GetComponent<Selectable>();
        selectable.transition = Selectable.Transition.None;

        // We look for a target if one is not selected
        image = (image == null) ? selectable.image : image;
        if (image == null)
        {
            Debug.LogError("No target image has been selected.", this);
            return;
        }
        image.color = defaultColor;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (debugMode) print("Enter");
        StartCoroutine(TransitionTo(hoverColor, fadeDuration));
        onEnter?.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (debugMode) print("Exit");
        StartCoroutine(TransitionTo(defaultColor, fadeDuration));
        onExit?.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (debugMode) print("Down");
        StartCoroutine(TransitionTo(downColor, fadeDuration));
        onDown?.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (debugMode) print("Up");
        Color targetColor = (eventData.pointerCurrentRaycast.gameObject != null) ? hoverColor : defaultColor;
        StartCoroutine(TransitionTo(targetColor, fadeDuration));
        onUp?.Invoke();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (debugMode) print("Click");
        StartCoroutine(TransitionTo(hoverColor, fadeDuration));
    }

    private IEnumerator TransitionTo(Color newColor, float fadeDuration)
    {
        float elapsedTime = 0.0f;
        while (elapsedTime < fadeDuration)
        {
            elapsedTime += GetDeltaTime();
            image.color = Color.Lerp(image.color, newColor, (elapsedTime / fadeDuration));
            yield return null;
        }
    }

    private float GetDeltaTime()
    {
        switch (deltaTimeType)
        {
            case DeltaTimeType.Fixed:
                return Time.fixedDeltaTime;

            case DeltaTimeType.Unscaled:
                return Time.unscaledTime;

            default:
            case DeltaTimeType.Default:
                return Time.deltaTime;
        }
    }
}