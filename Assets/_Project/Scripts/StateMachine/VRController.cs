using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRController : MonoBehaviour
{
#pragma warning disable 0414
    [TextArea(4, 10), SerializeField]
    private string componentDescription = "This component is responsible to hold together a set of components used for " +
        "the tracking state detection and as a way to control the state of the proxy hand as well.";
#pragma warning restore 0414

    [SerializeField] private OVRHand hand;

    private bool previousTrackingState;
    private Renderer handRenderer;
    private ProxyHand proxyHand;
    private OVRSkeleton skeleton;

    public OVRHand Hand { get => hand; }
    public OVRSkeleton Skeleton { get => skeleton; }
    public Renderer HandRenderer { get => handRenderer; set => handRenderer = value; }
    public bool PreviousTrackingState { get => previousTrackingState; set => previousTrackingState = value; }

    private void Awake()
    {
        handRenderer = hand.GetComponent<Renderer>();
        proxyHand = hand.GetComponentInChildren<ProxyHand>();
        skeleton = hand.GetComponent<OVRSkeleton>();
    }

    public void SetEnable(VRController controller, bool enable)
    {
        if (controller == this)
        {
            previousTrackingState = enable;
            this.gameObject.SetActive(enabled);
            if (proxyHand != null)
            {
                proxyHand.gameObject.SetActive(enabled);
                proxyHand.TeleportHand();
            }
        }
    }
}