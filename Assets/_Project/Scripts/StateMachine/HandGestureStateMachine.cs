using System.Collections.Generic;
using UnityEngine;

public class HandGestureStateMachine : BaseStateMachine
{
    [SerializeField] private OVRSkeleton.SkeletonType handType = OVRSkeleton.SkeletonType.None;
    [SerializeField] private Camera viewCamera = null;
    [SerializeField] private HandBonesData bonesData = null;
    [SerializeField] private HandPointer pointer = null;
    [Space]
    // This is meant to store in the list only the gestures
    // that we actually need in order to reduce the amount of checks necessary to identify the right gesture
    [SerializeField, ReadOnly] private List<HandState> cachedHandStates = new List<HandState>();

    private OVRHand hand = null;
    private TextFacingCamera debugText = null;
    private Renderer handRenderer = null;

    public OVRHand VRHand { get => hand; }
    public Renderer HandRenderer { get => handRenderer; }
    public TextFacingCamera DebugText { get => debugText; }
    public HandPointer Pointer { get => pointer; }
    public OVRSkeleton.SkeletonType HandType { get => handType; }
    public HandBonesData BonesData { get => bonesData; }
    public Camera ViewCamera { get => viewCamera; }
    public HandState CurrentState { get => currentState as HandState; }

    public delegate void OnEnterDelegate(HandState handState);
    public event OnEnterDelegate onEnterEvent;
    public event OnEnterDelegate onUpdateEvent;
    public event OnEnterDelegate onExitEvent;

    public void Initialize(IState state)
    {
        InitializeDebugText();
        InitializeHand();
        InitializeDefaultState(state);
        viewCamera = (viewCamera == null) ? Camera.main : viewCamera;
        if (BonesData == null)
        {
            bonesData = GetComponentInChildren<HandBonesData>();
            if (bonesData == null)
                Debug.LogError($"{this.gameObject.name} can't find an associated HandBonesData component.", this);
        }
    }

    private void InitializeDebugText()
    {
        debugText = GetComponentInChildren<TextFacingCamera>();
        if (debugText)
        {
            DebugText.Initialize();
            debugText.enabled = debugMode;
        }
    }

    public override void InitializeDefaultState(IState state)
    {
        base.InitializeDefaultState(state);

        if (state != null)
        {
            HandState newState = (state as HandState);
            newState.stateMachine = this;

            System.Type t = newState.GetType();

            HandState cachedState = newState.CreateCopy();
            cachedHandStates.Add(cachedState);
            newState = cachedState;

            currentState = newState;
            currentState.Enter();
            onEnterEvent?.Invoke(newState);
        }
    }

    private void InitializeHand()
    {
        hand = GetComponent<OVRHand>();
        handRenderer = hand.GetComponent<Renderer>();
    }

    protected override void Update()
    {
        base.Update();
        onUpdateEvent?.Invoke(currentState as HandState);
    }

    public void ChangeState(HandState handState)
    {
        if (handState == null)
        {
            Debug.LogError("Trying to Change State into null", this);
            return;
        }

        if (currentState != null)
        {
            currentState.Exit();
            onExitEvent?.Invoke(currentState as HandState);
            // Check if we already have our own copy of the Hand State
            HandState cachedState = GetCachedCopy(handState);
            if (cachedState == null)
            {
                // If we don't, we create a copy of it and we store a reference
                cachedState = handState.CreateCopy();
                cachedHandStates.Add(cachedState);
            }
            handState = cachedState;
            handState.stateMachine = this;
            currentState = handState;
            currentState.Enter();
            onEnterEvent?.Invoke(cachedState);
        }
    }

    public void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
        pointer.enabled = enabled;
    }

    private HandState GetCachedCopy(HandState newHandState)
    {
        foreach (HandState handState in cachedHandStates)
        {
            if (handState.name == newHandState.name)
                return handState;
        }
        return null;
    }
}