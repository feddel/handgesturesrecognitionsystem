using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class HandState : ScriptableObject, IState
{
    [SerializeField] protected HandStruct associatedHandStruct;
    [HideInInspector] public HandGestureStateMachine stateMachine;
    [SerializeField] protected List<HandState> possibleTransitions;

    public OVRSkeleton.SkeletonType HandType { get => stateMachine.HandType; }
    public List<HandState> PossibleTransitions { get => possibleTransitions; }
    public HandStruct AssociatedHandStruct { get => associatedHandStruct; }

    public virtual void Enter()
    { }

    public virtual void Exit()
    { }

    public virtual void Tick()
    { }

    public virtual void LateTick()
    { }

    public virtual void PhysicsTick()
    { }

    public virtual void InputsTick()
    { }

    /// <summary>
    /// Return a deep copy of the Hand State.
    /// I hate this solution, but for now this is the best I was able to come up with
    /// In the HandGestureStateMachine I need to generate a copy, but I receive the object as HandState
    /// and it the inspector I can't see the type, so each child class need to implement its version
    /// so that I can return the appropriate type.
    /// </summary>
    public abstract HandState CreateCopy();
}