public interface IState
{
    public abstract void Enter();

    public abstract void Exit();

    public abstract void Tick();

    public abstract void PhysicsTick();

    public abstract void LateTick();

    public abstract void InputsTick();
}