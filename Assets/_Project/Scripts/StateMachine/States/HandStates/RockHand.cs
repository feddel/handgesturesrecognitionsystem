using UnityEngine;

[CreateAssetMenu(fileName = "Rock State", menuName = "Hand States/Rock State")]
public class RockHand : HandState
{
    private HoldObject hold;
    private Vector3 startPosition;

    [Space]
    [SerializeField] private float distanceOffset = 0.075f;
    [SerializeField] private float speed = 0.025f;

    public override void Enter()
    {
        base.Enter();
        StartGrab();
    }

    public override void Exit()
    {
        EndGrab();
        base.Exit();
    }

    public override void Tick()
    {
        base.Tick();
        PerformGrab();
    }

    public void StartGrab()
    {
        Reset();

        GameObject hitObject = stateMachine.Pointer.HitObject;
        if (hitObject != null)
        {
            hold = hitObject.transform.GetComponent<HoldObject>();
            if (hold != null)
                startPosition = this.stateMachine.transform.position;
        }
    }

    public void PerformGrab()
    {
        if (hold != null)
        {
            float offset = Vector3.Distance(startPosition, this.stateMachine.transform.position);
            if (offset > distanceOffset)
            {
                LineDrawer.DrawLine(startPosition, stateMachine.Pointer.PointerLocation, Color.yellow);
                Vector3 directionVector = (stateMachine.Pointer.PointerLocation - startPosition);
                hold.transform.position += directionVector * speed;
            }
        }
    }

    public void EndGrab()
    {
        if (hold != null)
        {
            hold.OnDeselection(this);
            hold = null;
        }
    }

    private void Reset()
    {
        hold = null;
        startPosition = Vector3.zero;
    }

    public override HandState CreateCopy()
    {
        RockHand temp = ScriptableObject.CreateInstance<RockHand>();
        temp.stateMachine = this.stateMachine;
        temp.associatedHandStruct = this.AssociatedHandStruct;
        temp.possibleTransitions = this.possibleTransitions;
        temp.name = this.name;
        return temp;
    }
}