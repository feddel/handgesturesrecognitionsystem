using UnityEngine;

[CreateAssetMenu(fileName = "Pistol Up", menuName = "Hand States/Pistol Up")]
public class PistolUp : HandState
{
    public override HandState CreateCopy()
    {
        PistolUp temp = ScriptableObject.CreateInstance<PistolUp>();
        temp.stateMachine = this.stateMachine;
        temp.associatedHandStruct = this.AssociatedHandStruct;
        temp.possibleTransitions = this.possibleTransitions;
        temp.name = this.name;
        return temp;
    }
}