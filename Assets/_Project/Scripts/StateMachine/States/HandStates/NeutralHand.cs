using UnityEngine;

[System.Serializable, CreateAssetMenu(fileName = "Neutral State", menuName = "Hand States/Neutral State")]
public class NeutralHand : HandState
{
    public override void Enter() => base.Enter();

    public override void Exit() => base.Exit();

    public override void Tick() => base.Tick();

    public override void InputsTick() => base.InputsTick();

    public override HandState CreateCopy()
    {
        NeutralHand temp = ScriptableObject.CreateInstance<NeutralHand>();
        temp.stateMachine = this.stateMachine;
        temp.associatedHandStruct = this.AssociatedHandStruct;
        temp.possibleTransitions = this.possibleTransitions;
        temp.name = this.name;
        return temp;
    }
}