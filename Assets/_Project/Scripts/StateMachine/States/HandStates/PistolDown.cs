using UnityEngine;

[CreateAssetMenu(fileName = "Pistol Down", menuName = "Hand States/Pistol Down")]
public class PistolDown : HandState
{
    public override void Exit()
    {
        Deselect();
        base.Exit();
    }

    public override HandState CreateCopy()
    {
        PistolDown temp = ScriptableObject.CreateInstance<PistolDown>();
        temp.stateMachine = this.stateMachine;
        temp.associatedHandStruct = this.AssociatedHandStruct;
        temp.possibleTransitions = this.possibleTransitions;
        temp.name = this.name;
        return temp;
    }

    public void Deselect()
    {
        GameObject go = stateMachine.Pointer.HitObject;
        if (go != null)
        {
            ChangeMaterialInteraction target = go.transform.GetComponent<ChangeMaterialInteraction>();
            target?.OnSelection(this);
        }
    }
}