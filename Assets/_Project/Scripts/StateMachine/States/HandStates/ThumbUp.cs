using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ThumbUp State", menuName = "Hand States/ThumbUp State")]
public class ThumbUp : HandState
{
    // Very dirty display of gestures providing answer to a questions list
    private QuestionsTest t;

    private void Awake()
    {
        t = FindObjectOfType<QuestionsTest>();
    }

    public override void Enter()
    {
        base.Enter();
        t?.SetAnswer(1);
    }

    public override void Exit()
    {
        t?.SetAnswer(0);
        base.Exit();
    }

    public override HandState CreateCopy()
    {
        ThumbUp temp = ScriptableObject.CreateInstance<ThumbUp>();
        temp.stateMachine = this.stateMachine;
        temp.associatedHandStruct = this.AssociatedHandStruct;
        temp.possibleTransitions = this.possibleTransitions;
        temp.name = this.name;
        return temp;
    }
}