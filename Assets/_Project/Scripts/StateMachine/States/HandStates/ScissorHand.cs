using UnityEngine;

[CreateAssetMenu(fileName = "Scissor State", menuName = "Hand States/Scissor State")]
public class ScissorHand : HandState
{
    public override HandState CreateCopy()
    {
        ScissorHand temp = ScriptableObject.CreateInstance<ScissorHand>();
        temp.stateMachine = this.stateMachine;
        temp.associatedHandStruct = this.AssociatedHandStruct;
        temp.possibleTransitions = this.possibleTransitions;
        temp.name = this.name;
        return temp;
    }
}