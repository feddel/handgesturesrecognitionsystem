using UnityEngine;

[CreateAssetMenu(fileName = "Paper State", menuName = "Hand States/Paper State")]
public class PaperHand : HandState
{
    public override HandState CreateCopy()
    {
        PaperHand temp = ScriptableObject.CreateInstance<PaperHand>();
        temp.stateMachine = this.stateMachine;
        temp.associatedHandStruct = this.AssociatedHandStruct;
        temp.possibleTransitions = this.possibleTransitions;
        temp.name = this.name;
        return temp;
    }
}