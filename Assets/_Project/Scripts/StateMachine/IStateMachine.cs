public interface IStateMachine
{
    public abstract void InitializeDefaultState(IState state);

    public abstract void ChangeState(IState state);
}