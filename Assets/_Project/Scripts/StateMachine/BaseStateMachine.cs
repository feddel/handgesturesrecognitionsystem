using UnityEngine;

public abstract class BaseStateMachine : MonoBehaviour, IStateMachine
{
    public bool debugMode = false;
    public IState currentState;

    protected virtual void Update()
    {
        if (currentState != null)
        {
            currentState.InputsTick();
            currentState.Tick();
        }
    }

    protected virtual void FixedUpdate()
    {
        if (currentState != null)
            currentState.PhysicsTick();
    }

    protected virtual void LateUpdate()
    {
        if (currentState != null)
            currentState.LateTick();
    }

    public virtual void InitializeDefaultState(IState state)
    {
    }

    public virtual void ChangeState(IState state)
    {
        if (currentState != null)
        {
            currentState.Exit();
            currentState = state;
            currentState.Enter();
        }
    }
}