using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// VERY dirty test made in a few minutes in order to show the ability of the system 
/// to answer questions with gestures to a client showing up without notice
/// </summary>

public class QuestionsTest : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private Slider loading;
    [SerializeField] private List<string> questions;
    [SerializeField] private List<int> answers;

    private string currentQuestion = "";
    private int currentAnswer = 0;
    private int userAnswer = 0;
    private int currentIndex = 0;
    private float currentTimer = 0;
    private float maxTimer = 1.0f;

    private HandGestureStateMachine[] fsm;

    // Start is called before the first frame update
    private void Start()
    {
        fsm = FindObjectsOfType<HandGestureStateMachine>();

        currentQuestion = questions[currentIndex];
        currentAnswer = answers[currentIndex];

        text.text = currentQuestion;
    }

    // Update is called once per frame
    private void Update()
    {
        text.text = currentQuestion;
        if (userAnswer == currentAnswer)
        {
            currentTimer += Time.deltaTime;
            if (currentTimer >= maxTimer)
            {
                if (currentIndex + 1 == answers.Count) return;

                currentIndex++;

                currentQuestion = questions[currentIndex];
                currentAnswer = answers[currentIndex];
                userAnswer = 0;
                currentTimer = 0;
            }
            loading.value = currentTimer / maxTimer;
        }
        else
        {
            currentTimer = 0;
            loading.value = 0;
        }
    }

    public void SetAnswer(int i)
    {
        userAnswer = i;
    }
}